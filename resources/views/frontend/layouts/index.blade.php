<!DOCTYPE html>
<html lang="en">
    @include('frontend.layouts.head')
    <body>
    @yield('content')    
    @include('frontend.layouts.footer')
    @yield('script')
    </body>	
</html>
