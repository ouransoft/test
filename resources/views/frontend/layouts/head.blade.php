<head>
    <meta charset="utf-8"><style data-styles="">ion-icon{visibility:hidden}.hydrated{visibility:inherit}</style>
    <title>Ouransoft</title>
    <link rel="shortcut icon" href="https://ouransoft.vn/upload/logo-ouransoft.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="stylesheet" href="{!!asset('frontend/css/bootstrap.min.css')!!}">
    <link rel="stylesheet" href="{!!asset('frontend/css/owl.carousel.css')!!}">
    <link rel="stylesheet" href="{!!asset('frontend/css/animate.min.css')!!}">
    <link rel="stylesheet" href="{!!asset('frontend/css/fontawesome-all.css')!!}">
    <link rel="stylesheet" href="{!!asset('frontend/css/style-register.css')!!}">
    <link rel="stylesheet" href="{!!asset('frontend/css/style.css')!!}">
    <link rel="stylesheet" href="{!!asset('frontend/css/custom.css')!!}">
    <link rel="stylesheet" href="{!!asset('frontend/css/sweetalert2.min.css')!!}">
    <script src="{!!asset('frontend/js/jquery-3.3.1.min.js')!!}"></script>
    <script src="{!!asset('frontend/js/sweetalert2.min.js')!!}"></script>
</head>
