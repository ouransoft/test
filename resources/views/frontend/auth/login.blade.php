@extends('frontend.layouts.index')
@section('content')
<link rel="stylesheet" href="{!!asset('AdminLTE/plugins/toastr/toastr.min.css')!!}">
<div class="contaner">
    <div class="row" style="height: 100vh;">
        <div class="col-md-4"></div>
        <div class="col-md-4 d-flex flex-column justify-content-center" style="padding: 0 30px;">
            
            <div class="d-flex align-items-center w-100">
            
                <form action="{!!route('home.post-login')!!}" method="post" class="w-100 bg-white rounded-bottom px-4 py-4">
                <div class="w-100 rounded-top">
                    <h5 class="text-center text-light"><img src="{{asset('/img/img-logo.png')}}"></h5>
                </div>
                    @csrf
                    <div class="form-group mb-0">
                        <label for="name" class="pt-4 mb-0 pb-2">Email</label>
                        <input type="email" name='email' class="form-control px-1 pt-0 pb-0" id="name" required="" oninvalid="this.setCustomValidity('Nhập đúng thông tin email để đăng nhập')">
                    </div>
                    <div class="form-group mb-0">
                        <label for="password" class="pt-4 mb-0 pb-2">Mật khẩu</label>
                        <input type="password" name='password' class="form-control px-1 pt-0 pb-0" id="password" required="" oninvalid="this.setCustomValidity('Nhập thông tin password để đăng nhập')">
                    </div>
                    <div class='items-center justify-end mt-4'>
                        <a href="javascript:void(0)" class="underline text-sm text-gray-600 lh-36" id="forgot_password">Quên mật khẩu</a>
                        <a href="{!!route('home.get-register')!!}" class="underline text-sm text-gray-600 lh-36 register float-right">Đăng ký tài khoản</a>
                        
                    </div>
                    <div class='items-center text-center' style="display:grid">
                        <button type="submit" class="btn btn-primary border-0" style="background: #ff7700;">Đăng nhập</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4"></div>     
    </div>
</div>
<div class="modal fade" id="ForgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Gửi yêu cầu đổi mật khẩu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form style="box-shadow:none;" id="frmForgotPassword">
      <div class="modal-body">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Email:</label>
            <input type="text" class="form-control" name="email" id="recipient-name" require>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
        <button type="submit" class="btn btn-primary">Gửi yêu cầu</button>
      </div>
      </form>
    </div>
  </div>
</div>
@stop
@section('script')
@parent
<script src={{ asset('AdminLTE/plugins/toastr/toastr.min.js') }}></script>
<script>
     $('#forgot_password').click(function(){
         $('#ForgotPasswordModal').modal('show');
     })
     $('#frmForgotPassword').submit(function(e){
        e.preventDefault();
        $this =  $(this);
        $.ajax({
                    url:'/api/forgot-password',
                    method:'POST',
                    data: $this.serialize(),
                    success: function (response){
                        if(response.success == true){
                            $('#ForgotPasswordModal').modal('hide');
                            Swal.fire({
                                title: 'Gửi yêu cầu đổi mật khẩu thành công',
                                text: "Bạn vui lòng truy cập email đã đăng ký để xác nhận yêu cầu",
                                icon: 'success',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Đến hòm thư'
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    window.open('https://mail.google.com/');
                                }
                            })
                        }
                    }
                })
     })
</script>
@if (Session::get('info'))
<script>
    toastr.info('{{ Session::get('info') }}')
</script>
@endif
@stop
