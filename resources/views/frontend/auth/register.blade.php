@extends('frontend.layouts.index')
@section('content')
<link rel="stylesheet" href="{!!asset('AdminLTE/plugins/toastr/toastr.min.css')!!}">
<div class="contaner">
    <div class="row" style="height: 100vh;">
        <div class="col-md-4"></div>
        <div class="col-md-4 d-flex flex-column justify-content-center">
            <div class="w-100 py-4 rounded-top" style="background: #ff7700;">
                <h5 class="text-center text-light">Vui lòng đăng ký tài khoản để tham gia</h5>
            </div>
            <div class="d-flex align-items-center w-100">
                <form action="{!!route('home.post-register')!!}" method="post" class="w-100 bg-white rounded-bottom px-4 py-4">
                    @csrf
                    <div class="form-group mb-0">
                        <label for="name" class="pt-4 mb-0 pb-2">Họ tên</label>
                        <input type="text" name='name' class="form-control px-1 pt-0 pb-0" id="name" required="">
                    </div>
                    <div class="form-group mb-0">
                        <label for="phone" class="pt-4 mb-0 pb-2">Số điện thoại</label>
                        <input type="text" name='phone' class="form-control px-1 pt-0 pb-0" id="phone" required="">
                    </div>
                    <div class="form-group mb-0">
                        <label for="email" class="pt-4 mb-0 pb-2">Email</label>
                        <input type="email" name='email' class="form-control px-1 pt-0 pb-0" id="email" required="">
                    </div>
                    <div class="form-group mb-0">
                        <label for="code" class="pt-4 mb-0 pb-2">Mã công ty</label>
                        <input type="text" name='code' class="form-control px-1 pt-0 pb-0" id="code" required="">
                    </div>
                    <div class="form-group mb-0">
                        <label for="password" class="pt-4 mb-0 pb-2">Mật khẩu</label>
                        <input type="password" name='password' class="form-control px-1 pt-0 pb-0" id="password" required="">
                    </div>
                    <div class="form-group mb-0 pt-4">
                        <span class="pb-2">Giới tính</span>
                        <div class="d-flex pt-3">
                            <label class="rad-label mb-0">
                                <input type="radio" class="rad-input" name="gender" checked="" value='1'>
                                <div class="rad-design"></div>
                                <div class="rad-text">Nam</div>
                            </label>
                            <label class="rad-label ml-3 mb-0">
                                <input type="radio" class="rad-input" name="gender" value='2'>
                                <div class="rad-design"></div>
                                <div class="rad-text">Nữ</div>
                            </label>
                        </div>
                    </div>
                    <div class='d-flex items-center justify-end mt-4'>
                        <a href="{!!route('home.get-login')!!}" class="underline text-sm text-gray-600 lh-36">Đã có tài khoản</a>
                        <button type="submit" class="btn btn-primary border-0 ml-2" style="background: #ff7700;">Đăng ký</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4"></div>     
    </div>
</div>
@stop
@section('script')
@parent
<script src={{ asset('AdminLTE/plugins/toastr/toastr.min.js') }}></script>
@if (Session::get('info'))
<script>
    toastr.info('{{ Session::get('info') }}')
</script>
@endif
@stop
