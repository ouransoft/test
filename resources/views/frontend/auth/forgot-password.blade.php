@extends('frontend.layouts.index')
@section('content')
<link rel="stylesheet" href="{!!asset('AdminLTE/plugins/toastr/toastr.min.css')!!}">
<div class="contaner">
    <div class="row" style="height: 100vh;">
        <div class="col-md-4"></div>
        <div class="col-md-4 d-flex flex-column justify-content-center" style="padding: 0 30px;">
            <div class="w-100 py-4 rounded-top" style="background: #ff7700;">
                <h5 class="text-center text-light">Mật khẩu mới</h5>
            </div>
            <div class="d-flex align-items-center w-100">
                <form id='frmForgotPassword' method="post" class="w-100 bg-white rounded-bottom px-4 py-4">
                    @csrf
                    <div class="form-group mb-0">
                        <label for="name" class="pt-4 mb-0 pb-2">Mật khẩu</label>
                        <input type="password" name='password' class="form-control px-1 pt-0 pb-0" id="name" required="">
                    </div>
                    <div class='d-flex items-center justify-end mt-4'>
                        <a href="{!!route('home.get-login')!!}" class="underline text-sm text-gray-600 lh-36">Đăng nhập</a>
                        <button type="submit" class="btn btn-primary border-0 ml-2" style="background: #ff7700;">Cập nhật</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4"></div>     
    </div>
</div>
@stop
@section('script')
@parent
<script src={{ asset('AdminLTE/plugins/toastr/toastr.min.js') }}></script>
<script>
     $('#frmForgotPassword').submit(function(e){
        e.preventDefault();
        $this =  $(this);
        $.ajax({
                    url:'{{route('home.post-forgot-password',$_GET)}}',
                    method:'POST',
                    data: $this.serialize(),
                    success: function (response){
                        if(response.success == true){
                            toastr.success('Thay đổi mật khẩu thành công');
                            setTimeout(function(){ 
                                location.href = '{!!route('home.get-login')!!}'; 
                                }, 3000);
                        }
                    }
                })
     })
</script>
@if (Session::get('info'))
<script>
    toastr.info('{{ Session::get('info') }}')
</script>
@endif
@stop
