@extends('frontend.layouts.index')
@section('content')
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="{!!asset('frontend/css/style-result.css')!!}"/>
<link rel="stylesheet" type="text/css" href="{!!asset('frontend/css/slick.css')!!}"/>
<link rel="stylesheet" type="text/css" href="{!!asset('frontend/css/slick-theme.css')!!}"/>

<div class="container" id="result-page">
    <div class="row">
        <a href="{{route('home.index')}}" class="position-absolute back-btn w-auto" style="text-decoration: none; font-size: 18px">
            <span class="d-flex align-items-center mt-4 rounded px-2 py-1" title="NEXT" style="color: #fff; background: #ff7700;"><ion-icon name="home" class="pr-1"></ion-icon><span>Trang chủ</span></span>
        </a>
        <div class="slide-title col-md-6 mx-auto">
            <div class="">
                <h3 class="mb-0 mt-4 text-center" style="color: #ff7700;">Kết quả đánh giá</h3>
                <div id="chart"></div>
            </div>

        </div>
    </div>
    <nav>
        <div class="nav nav-tabs flex-nowrap" id="nav-tab" role="tablist">
          <button class="nav-link text-dark active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#overview" type="button" role="tab" aria-controls="overview" aria-selected="true">Tổng quan</button>
          <button class="nav-link text-dark" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#relationship" type="button" role="tab" aria-controls="relationship" aria-selected="false">Đặc trưng</button>
          <button class="nav-link text-dark" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#works" type="button" role="tab" aria-controls="works" aria-selected="false">Ưu điểm</button>
          <button class="nav-link text-dark" id="nav-bonus-tab" data-bs-toggle="tab" data-bs-target="#bonus" type="button" role="tab" aria-controls="bonus" aria-selected="false">Nhược điểm</button>
        </div>
    </nav>
    <div class="tab-content mb-4" id="nav-tabContent">
        <div class="tab-pane py-3 px-3 fade show active" id="overview" role="tabpanel" aria-labelledby="nav-home-tab">{!!$personality->description_code!!}</div>
        <div class="tab-pane py-3 px-3 fade" id="relationship" role="tabpanel" aria-labelledby="nav-profile-tab">{!!$personality->featured!!}</div>
        <div class="tab-pane py-3 px-3 fade" id="works" role="tabpanel" aria-labelledby="nav-contact-tab">{!!$personality->advantages!!}</div>
        <div class="tab-pane py-3 px-3 fade" id="bonus" role="tabpanel" aria-labelledby="nav-bonus-tab">{!!$personality->defects!!}</div>
    </div>
</div>

@stop
@section('script')
@parent
<script type="text/javascript" src="{!!asset('frontend/js/slick.min.js')!!}"></script>
<script type="text/javascript" src="{!!asset('frontend/js/apexcharts.min.js')!!}"></script>
<script>
    $('body').css('background','#f9f9f9');
    // $('#myModal').modal('show');
    $(window).load(function(){
        $('#congrat-modal').css('display','block');
        $('body').css('overflow','hidden');
    })
    $('#congrat-modal').click(function(){
        $('body').css('overflow','scroll');
        $(this).css('display','none');
    })
    $('.close-btn').click(function(){
        $('body').css('overflow','scroll');
        $('#congrat-modal').css('display','none');
    })
    //slider
    $('.slider-nav').slick({
        centerMode: false,
        // centerPadding: '100px',
        slidesToShow: 4,
        slidesToScroll: 4,
        focusOnSelect: true,
        infinite: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    centerPadding: '100px',
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerPadding: '100px',
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
                {
                breakpoint: 480,
                settings: {
                    centerMode: true,
                    // infinite: true,
                    centerPadding: '50px',
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ],
        asNavFor: '.slider-for'
    });
    $('.slider-for').slick({
        speed: 400,
        adaptiveHeight: true,
        settings: "unslick",
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });

    //chart
    console.log({!!implode(',',$dataChart)!!});
    var options = {
        series: [{
            name: 'Tổng điểm',
            data: [{!!implode(',',$dataChart)!!}],
        }],
        chart: {
            height: 350,
            type: 'radar',
            toolbar: {
                show: false
            }
        },
        xaxis: {
            categories: ['Thực thi (IM)', 'Điều phối (CO)', 'Khuôn phép (SH)', 'Sáng tạo (PL)', 'Khám phá (RI)', 'Điều hành (ME)','Đoàn kết (TW)','Hoàn thiện (CF)']
        },
        stroke: {
            colors: ['#ff7700'],
        },
        markers: {
            size: 5,
            colors: ['#ff7700', '#ff7700', '#ff7700']
        },
        fill: {
            colors: ['#F44336', '#E91E63', '#9C27B0']
        },
        plotOptions: {
            radar: {
                polygons: {
                    strokeColor: ['#ff7700'],
                    fill: {
                        colors: ['#fff', '#fff']
                    }
                }
            }
        }
    };

    var chart = new ApexCharts(document.querySelector("#chart"), options);
    chart.render();
</script>
@stop
