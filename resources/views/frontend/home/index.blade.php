@extends('frontend.layouts.index')
@section('content')
<div class="home container pb-4">
    <div class="img-member mt-2 text-center" style="margin-top: 30px;position:relative">
        <a href="{{route('home.logout')}}" class="position-absolute back-btn w-auto" style="text-decoration: none; font-size: 18px;right:-10px;top:-27px;">
            <span class="d-flex align-items-center mt-4 rounded px-2 py-1" title="Đăng xuất" style="color: #fff; background: #797979;"><span>Đăng xuất</span></span>
        </a>
        <img class="rounded-circle" src="https://ui-avatars.com/api/?name={!!\Auth::guard('member')->user()->name!!}&background=ff7700&color=fff&size=100" style="height: 60px">
    </div>
    <div class="title-welcome mt-2">
        <h3 class="text-center">Chào mừng {!!\Auth::guard('member')->user()->name!!}</h3>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-4 d-flex align-items-end">
            <div class="w-100 rounded mt-3" style="background: #fff;box-shadow: 2px 4px 12px -5px rgba(0,0,0,0.59); border: 1px solid #252525">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#test1">
                    <span class="text-light font-weight-bold px-1 py-1 position-absolute rounded" style="font-size: 11px; bottom: 68px; left: 10%; background:#33ac3d">70 câu hỏi</span>
                    <span class="text-light font-weight-bold px-1 py-1 position-absolute rounded" style="font-size: 11px; bottom: 68px; right: 10%; background:#ad36ea">Trắc nghiệm</span>
                    <div class="image rounded-top" style="height: 190px; overflow: hidden;">
                        <img class="w-100" src="{!!asset('/frontend/img/anh10.jpg')!!}" alt="" style="min-height: 100%;">
                    </div>
                    <div class="content pb-2">
                        <div class="title" style="border-top: 1px solid #7d7d7d;">
                            <h5 class="text-dark pl-3 pb-1 pt-3">Bài test tính cách</h5>
                        </div>
                        <div class="extra d-flex ">
                            <span class="text-dark pl-3 d-flex align-items-center" style="font-size: 14px;font-size: 14px;">
                                <p class="mb-0 pr-1">Bắt đầu test</p>
                                <ion-icon name="arrow-forward-outline" role="img" class="md hydrated" aria-label="arrow forward outline"></ion-icon>
                            </span>
                            @if(count(\Auth::guard('member')->user()->histotyMbti) > 0)
                            <a href="{!!route('home.mbti.result')!!}" class="py-1 px-2 text-light recent-result rounded" style="background: #ff7700; top:24px; left:10px">Kết quả test gần đây</a>
                            @endif
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 d-flex align-items-end" style="position:relative">
            <div class="w-100 rounded mt-3" style="background: #fff;box-shadow: 2px 4px 12px -5px rgba(0,0,0,0.59); border: 1px solid #252525">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#test2">
                    <span class="text-light font-weight-bold px-1 py-1 position-absolute rounded" style="font-size: 11px; bottom: 68px; left: 10%; background:#33ac3d">35 câu hỏi</span>
                    <span class="text-light font-weight-bold px-1 py-1 position-absolute rounded" style="font-size: 11px; bottom: 68px; right: 10%; background:#ad36ea">Trắc nghiệm</span>
                    <div class="image rounded-top" style="height: 190px; overflow: hidden;">
                        <img class="w-100" src="{!!asset('/frontend/img/anh6.jpg')!!}" alt="" style="min-height: 100%;">
                    </div>
                    <div class="content pb-2">
                        <div class="title" style="border-top: 1px solid #7d7d7d;">
                            <h5 class="text-dark pl-3 pb-1 pt-3">Bài test phong cách lãnh đạo</h5>
                        </div>
                        <div class="extra d-flex justify-content-between">
                            <span class="text-dark pl-3 d-flex align-items-center" style="font-size: 14px;">
                                <p class="mb-0 pr-1">Bắt đầu test</p>
                                <ion-icon name="arrow-forward-outline" role="img" class="md hydrated" aria-label="arrow forward outline"></ion-icon>
                            </span>
                            @if(count(\Auth::guard('member')->user()->histotyLeadership) > 0)
                            <a href="{!!route('home.leadership.result')!!}" class="py-1 px-2 text-light recent-result rounded" style="background: #ff7700; top:24px; left:10px">Kết quả test gần đây</a>
                            @endif
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-2"></div>
        <div class="col-md-4">
            <div class="w-100 rounded mt-3" style="background: #fff;box-shadow: 2px 4px 12px -5px rgba(0,0,0,0.59); border: 1px solid #252525">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#test3">
                    <span class="text-light font-weight-bold px-1 py-1 position-absolute rounded" style="font-size: 11px; bottom: 68px; left: 10%; background:#33ac3d">07 câu hỏi</span>
                    <span class="text-light font-weight-bold px-1 py-1 position-absolute rounded" style="font-size: 11px; bottom: 68px; right: 10%; background:#ad36ea">Xếp điểm</span>
                    <div class="image rounded-top" style="height: 190px; overflow: hidden;">
                        <img class="rounded w-100" src="{!!asset('/frontend/img/anh7.jpg')!!}" alt="" style="min-height: 100%;">
                    </div>
                    <div class="content pb-2">
                        <div class="title" style="border-top: 1px solid #7d7d7d;">
                            <h5 class="text-dark pl-3 pb-1 pt-3">Bài test kĩ năng làm việc nhóm</h5>
                        </div>
                        <div class="extra d-flex justify-content-between">
                            <span class="text-dark pl-3 d-flex align-items-center" style="font-size: 14px;">
                                <p class="mb-0 pr-1">Bắt đầu test</p>
                                <ion-icon name="arrow-forward-outline" role="img" class="md hydrated" aria-label="arrow forward outline"></ion-icon>
                            </span>
                            @if(count(\Auth::guard('member')->user()->histotyTeamWork) > 0)
                            <a href="{!!route('home.teamwork.result')!!}" class="py-1 px-2 text-light recent-result rounded" style="background: #ff7700; top:24px; left:10px">Kết quả test gần đây</a>
                            @endif
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="w-100 rounded mt-3" style="background: #fff;box-shadow: 2px 4px 12px -5px rgba(0,0,0,0.59); border: 1px solid #252525">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#test4">
                    <span class="text-light font-weight-bold px-1 py-1 position-absolute rounded" style="font-size: 11px; bottom: 68px; right: 10%; background:#ad36ea">Xếp thứ tự</span>
                    <div class="image rounded-top" style="height: 190px; overflow: hidden;">
                        <img class="w-100" src="{!!asset('/frontend/img/anh8.png')!!}" alt="" style="min-height: 100%;">
                    </div>
                    <div class="content pb-2">
                        <div class="title" style="border-top: 1px solid #7d7d7d;">
                            <h5 class="text-dark pl-3 pb-1 pt-3">Khảo sát nhanh</h5>
                        </div>
                        <div class="extra d-flex justify-content-between">
                            <span class="text-dark pl-3 d-flex align-items-center" style="font-size: 14px;">
                                <p class="mb-0 pr-1">Bắt đầu test</p>
                                <ion-icon name="arrow-forward-outline" role="img" class="md hydrated" aria-label="arrow forward outline"></ion-icon>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="test1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header pb-2" style="background: #ff7700;">
                <h5 class="modal-title text-light d-flex align-items-center" id="exampleModalLongTitle">
                    <span class="pr-1">Chi tiết bài test</span>
                    <ion-icon name="book" role="img" class="md hydrated" aria-label="book"></ion-icon>
                </h5>
                <button type="button" class="bg-transparent border-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-white">
                        <ion-icon name="close-outline" style="font-size: 30px;" role="img" class="md hydrated" aria-label="close outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body d-flex flex-column">
                <span>Bài test gồm 70 câu</span>
                <span>Hình thức: trắc nghiệm</span>
                <span>Thời gian: 15 phút</span>
            </div>
            <div class="modal-footer d-flex text-right">
                <a href="{!!route('home.mbti')!!}" class="text-white btn btn-key">Bắt đầu </a>
            </div>
        </div>
    </div>
</div>
{{-- test 2 --}}
<div class="modal fade" id="test2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header pb-2" style="background: #ff7700;">
                <h5 class="modal-title text-light d-flex align-items-center" id="exampleModalLongTitle">
                    <span class="pr-1">Chi tiết bài test</span>
                    <ion-icon name="book" role="img" class="md hydrated" aria-label="book"></ion-icon>
                </h5>
                <button type="button" class="bg-transparent border-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-white">
                        <ion-icon name="close-outline" style="font-size: 30px;" role="img" class="md hydrated" aria-label="close outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body d-flex flex-column">
                <span>Bài test gồm 35 câu</span>
                <span>Hình thức: trắc nghiệm</span>
                <span>Thời gian: 5 phút</span>
            </div>
            <div class="modal-footer d-flex text-right">
                <a href="{!!route('home.leadership')!!}" class="text-white btn btn-key">Bắt đầu</a>
            </div>
        </div>
    </div>
</div>
{{-- test 3 --}}
<div class="modal fade" id="test3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header pb-2" style="background: #ff7700;">
                <h5 class="modal-title text-light d-flex align-items-center" id="exampleModalLongTitle">
                    <span class="pr-1">Chi tiết bài test</span>
                    <ion-icon name="book" role="img" class="md hydrated" aria-label="book"></ion-icon>
                </h5>
                <button type="button" class="bg-transparent border-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-white">
                        <ion-icon name="close-outline" style="font-size: 30px;" role="img" class="md hydrated" aria-label="close outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body d-flex flex-column">
                <span>Bài test gồm 7 câu</span>
                <span>- Hình thức: Xếp điểm </span>
                <span>- Bạn có mười (10) điểm để phân chia vào các ý ở bên trái và  tùy thuộc vào bao nhiêu điểm bạn đồng ý với những ý đó.</span>
                <span>- Những ý bạn không đồng ý hoặc là không quan tâm đến sẽ được tính như 0 điểm. </span>
                <span>- Bạn không thể cho điểm âm với bất kì ý nào.</span>
                <span>- Bạn cần chia hết 10 điểm mỗi câu để hoàn thành bài test.</span>
                <span>- Hãy phân chia điểm như bạn muốn.</span>
                <span> Thời gian: 10 phút </span>
            </div>
            <div class="modal-footer d-flex text-right">
                <a href="{!!route('home.teamwork')!!}" class="text-white btn btn-key">Bắt đầu </a>
            </div>
        </div>
    </div>
</div>
{{-- test 4 --}}
<div class="modal fade" id="test4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header pb-2" style="background: #ff7700;">
                <h5 class="modal-title text-light d-flex align-items-center" id="exampleModalLongTitle">
                    <span class="pr-1">Chi tiết bài test</span>
                    <ion-icon name="book" role="img" class="md hydrated" aria-label="book"></ion-icon>
                </h5>
                <button type="button" class="bg-transparent border-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-white">
                        <ion-icon name="close-outline" style="font-size: 30px;" role="img" class="md hydrated" aria-label="close outline"></ion-icon>
                    </span>
                </button>
            </div>
            <div class="modal-body d-flex flex-column">
                <h5>Khảo sát về tầm quan trọng:</h5>
                <span>- Hình thức: Bạn hãy sắp xếp 9 đặc điểm của từng công việc (không trùng nhau), theo tầm quan trọng mà bạn muốn gán cho từng đặc điểm:</span>
                <span>1 = Quan trọng nhất</span>
                <span>9 = Ít quan trọng nhất</span>
                <span>- Thời gian: 5 phút</span>
            </div>
            <div class="modal-footer border-bottom border-top-0 pt-0 d-flex justify-content-between">
                <a href="{!!route('home.quick')!!}" class="text-white btn btn-key">Bắt đầu </a>
            </div>
            <div class="modal-body d-flex flex-column">
                <h5 class="mt-2">Khảo sát về các mức độ đánh giá:</h5>
                <span>- Hình thức: Đánh giá 16 yếu tố theo mức độ quan trọng từ</span>
                <span>0 - Không quan trọng</span>
                <span>10 - Rất quan trọng</span>
                <span>- Thời gian: 5 phút</span>
            </div>
            <div class="modal-footer border-bottom border-top-0 pt-0 d-flex justify-content-between">
                <a href="{!!route('home.quick2')!!}" class="text-white btn btn-key">Bắt đầu </a>
            </div>
        </div>
    </div>
</div>
@stop
