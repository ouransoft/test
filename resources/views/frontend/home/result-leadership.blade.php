@extends('frontend.layouts.index')
@section('content')
<style>
    #container {
        height: 400px;
    }

    .highcharts-figure, .highcharts-data-table table {
      min-width: 310px;
      max-width: 800px;
      margin: 1em auto;
    }

    .highcharts-data-table table {
      font-family: Verdana, sans-serif;
      border-collapse: collapse;
      border: 1px solid #EBEBEB;
      margin: 10px auto;
      text-align: center;
      width: 100%;
      max-width: 500px;
    }
    .highcharts-data-table caption {
      padding: 1em 0;
      font-size: 1.2em;
      color: #555;
    }
    .highcharts-data-table th {
            font-weight: 600;
      padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
      padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
      background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
      background: #f1f7ff;
    }
</style>
<div class="container" id="result-page">
    <div id="congrat-modal">
        <div class="container h-100 d-flex align-items-center">
            <div class="col-md-4 bg-white mx-auto rounded d-flex align-items-center flex-column">
                <span class="close-btn pr-2 text-danger">&times;</span>
                <span class="text-center py-3">
                    <ion-icon name="checkmark-circle" style="font-size: 120px; color: rgb(106, 250, 40)"></ion-icon>
                </span>
                <p class="text-success text-center">Chúc mừng bạn đã hoàn thành bài thi.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <a href="{{route('home.index')}}" class="position-absolute back-btn" style="text-decoration: none; font-size: 18px">
            <span class="d-flex align-items-center mt-4 rounded px-2 py-1" title="NEXT" style="color: #fff; background: #ff7700;"><ion-icon name="home" class="pr-1"></ion-icon><span>Trang chủ</span></span>
        </a>
        <div class="slide-title col-md-6 mx-auto">
            <div class="">
                <h3 class="mb-0 mt-4 text-center" style="color: #ff7700;">Kết quả đánh giá</h3>
                <figure class="highcharts-figure">
                    <div id="container"></div>
                </figure>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script src="{!!asset('frontend/js/highcharts.js')!!}"></script>
<script src="{!!asset('frontend/js/exporting.js')!!}"></script>
<script src="{!!asset('frontend/js/export-data.js')!!}"></script>
<script src="{!!asset('frontend/js/accessibility.js')!!}"></script>
<script>
    // Data gathered from http://populationpyramid.net/germany/2015/

// Age categories
var categories = [
  '{!!\Auth::guard('member')->user()->name!!}'
];

Highcharts.chart('container', {
  chart: {
    type: 'bar'
  },
  title: {
    text: 'Biểu đồ thống kê phong cách lãnh đạo'
  },
  subtitle: {
    text: ''
  },
  accessibility: {
    point: {
      valueDescriptionFormat: '{index}. Age {xDescription}, {value}%.'
    }
  },
  xAxis: [{
    categories: categories,
    reversed: false,
    labels: {
      step: 1
    },
    accessibility: {
      description: 'Age (male)'
    }
  }],
  yAxis: {
    title: {
      text: null
    },
    labels: {
      formatter: function () {
        return Math.abs(this.value);
      }
    },
    accessibility: {
      description: 'Percentage population',
      rangeDescription: 'Range: 0 to 5%'
    }
  },

  plotOptions: {
    series: {
      stacking: 'normal',
      dataLabels: {
                enabled: false
       }
    }
  },

  tooltip: {
    formatter: function () {
      return '<b>' + this.series.name + ' ' + this.point.category + '</b><br/>' +
        'Điểm: ' + Highcharts.numberFormat(Math.abs(this.point.y), 1);
    }
  },

  series: [{
    name: 'Phong cách lãnh đạo con người',
    color: '#f47620',
    displayNegative: true,
    negativeColor: '#f47620'  ,
    data: [
      -{!!$history->human!!}
    ]
  }, {
    name: 'Phong cách lãnh đạo công việc',
    color: 'blue',
    displayNegative: true,
    negativeColor: 'blue'  ,
    data: [
      {!!$history->work!!}
    ]
  }]
});
</script>
@stop
