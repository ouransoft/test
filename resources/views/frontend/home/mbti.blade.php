@extends('frontend.layouts.index')
@section('content')
<div class="wrapper position-relative d-flex">
    <div class="wrapper menu position-relative w-25">
        <h5 class="question-title position-absolute w-100 text-center pt-2 pb-2 border-bottom border-muted">Danh sách câu hỏi</h5>
        <div class="bg-white border-right border-warning py-5 pl-2 h-100">
            {{-- <h5 class="pt-2 mr-2">Thời gian còn lại: <span id="time" class="text-warning">05:00</span></h5> --}}
            <div class="pt-2 d-flex align-content-start flex-wrap ">
                @foreach($questions as $key=>$val)
                <div class="mr-1 mt-2 quest-item">
                    <a href="javascript:void(0)" class="choose-question" data-index='{{$key}}' >
                        <h6 class="border border-info rounded px-1 text-center py-2 bg-muted" id='question{{$key}}'>{{++$key}}</h6>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div>
        <span class="menu-remote">
            <ion-icon name="reorder-four-outline" class="pl-2 pt-2"></ion-icon>
        </span>
    </div>
    <div class="wizard-content-1 clearfix pt-5 w-75 mx-auto">
        <div class="steps d-inline-block position-absolute clearfix">
            <ul class="tablist multisteps-form__progress">
                <li class="multisteps-form__progress-btn js-active current"></li>
                <li class="multisteps-form__progress-btn "></li>
                <li class="multisteps-form__progress-btn"></li>
                <li class="multisteps-form__progress-btn"></li>
                <li class="multisteps-form__progress-btn"></li>
            </ul>
        </div>
        <div class="step-inner-content clearfix position-relative">
            {{-- <form class="multisteps-form__form" action="https://jthemes.net/themes/html/BeWizard/Quiz/thank-you.html" id="wizard" method="POST"> --}}
                <div class="form-area position-relative">
                    <div class="multisteps-form__panel js-active" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative pt-0 pb-3 mx-2 rounded" id="mbti">
                        </div>
                    </div>
                </div>
            {{-- </form> --}}
        </div>
    </div>
</div>
@stop
@section('script')
@parent
<script>
    !function() {
        function Mbti(selector, events) {
          this.el = document.querySelector(selector);
          this.events = events;
          this.index = 0;
          this.data = this.events[0];
          this.draw();
        }
        var result = {};
        Mbti.prototype.draw = function() {
            this.drawTitle();
            this.drawAnswer();
            this.drawFooter();
        }
        Mbti.prototype.drawTitle = function() {
            var self = this;
            let data = self.data;
            let index = self.index;
            if(!this.header) {
                this.header = createElement('div','quiz-title text-center');
                //
                this.upperheader = createElement('p','countdown d-flex align-items-center justify-content-between mb-4 pt-3');
                this.previous = createElement('div','li');
                this.previous.addEventListener('click', function() { self.previousQuestion(); });
                this.previous.innerHTML = '<a href="javascript:void(0)"><span class="d-flex align-items-center text-dark" title="PREV"><ion-icon name="chevron-back"></ion-icon>Quay lại</span></a>';
                this.time = createElement('span','d-flex align-items-center');
                this.time.innerHTML = '<span class="d-flex align-items-center"><ion-icon name="alarm-outline" class="pr-2 text-danger clock"></ion-icon><span id="time" class="text-dark">15:00</span></span>';
                this.next = createElement('div','li');
                this.next.addEventListener('click', function() { self.nextQuestion(); });
                this.next.innerHTML = '<a href="javascript:void(0)"><span class="d-flex align-items-center text-dark" title="NEXT">Tiếp theo<ion-icon name="chevron-forward"></ion-icon></span></a>';
                this.upperheader.appendChild(this.previous);
                this.upperheader.appendChild(this.time);
                this.upperheader.appendChild(this.next);
                //
                // this.title = createElement('span');
                this.content = createElement('p');
                //Append the Elements
                this.header.appendChild(this.upperheader);
                // this.header.appendChild(this.title);
                this.header.appendChild(this.content);
                this.el.appendChild(this.header);
            }
            // this.title.innerHTML = 'Câu hỏi '+(index + 1);
            this.content.innerHTML = (index + 1) +". "+data.question;
        };
        Mbti.prototype.drawAnswer = function() {
            let self = this;
            let data = self.data;
            if(!this.answer) {
                this.answer = createElement('div','quiz-option-selector pt-2 pb-4');
                //
                this.answer1 = createElement('label','quiz-details-option position-relative mt-2 py-3 pr-2 pl-5 w-100');
                //
                this.answer2 = createElement('label','quiz-details-option position-relative mt-2 py-3 pr-2 pl-5 w-100');
                this.answer.appendChild(this.answer1);
                this.answer.appendChild(this.answer2);
                this.el.appendChild(this.answer);
            }
            if(result[this.index]){
                if(result[this.index] == self.data.type.charAt(0)){
                    check_answer1 = 'checked';
                    check_answer2 = '';
                }else{
                    check_answer1 = '';
                    check_answer2 = 'checked';
                }
            }else{
                check_answer1 = '';
                check_answer2 = '';
            }
            this.answer1.innerHTML = ` <input type="radio" name="quiz" value="`+ self.data.type.charAt(0) +`" class="exp-option-box" `+check_answer1+`>
                                        <span class="select-area"></span>
                                        <span>`+data.answer1+`<span>`;
            this.answer2.innerHTML = ` <input type="radio" name="quiz" value="`+ self.data.type.charAt(1) +`" class="exp-option-box" `+check_answer2+`>
                                        <span class="select-area"></span>
                                        <span>`+data.answer2+`<span>`;
            document.querySelectorAll('.exp-option-box').forEach(item => {
                item.addEventListener('change', event => {
                   self.chooseAnswer();
                })
            })


        };
        Mbti.prototype.drawFooter = function() {
            let self = this;
            if(!this.footer){
                this.footer = createElement('div','actions mt-5 d-flex justify-content-between align-items-center mx-auto');
                // var previous = createElement('div','li ml-3 mt-1');
                // previous.addEventListener('click', function() { self.previousQuestion(); });
                // previous.innerHTML = '<span class="js-btn-next" title="PREV"><ion-icon name="chevron-back-circle"></ion-icon></span>';
                this.progress = createElement('div','quiz-progress-area w-100 d-flex align-items-center')
                // var next = createElement('div','li ml-3 mt-1');
                // next.addEventListener('click', function() { self.nextQuestion(); });
                // next.innerHTML = '<span class="js-btn-next" title="NEXT"><ion-icon name="chevron-forward-circle"></ion-icon></span>';
                // this.footer.appendChild(previous);
                this.footer.appendChild(this.progress);
                // this.footer.appendChild(next);
                this.el.appendChild(this.footer);
            }
            if(result){
                count_result = Object.keys(result).length;
            }else{
                count_result = 0;
            }
            let progress = (count_result / this.events.length)*100;
            this.progress.innerHTML =  `<h3 class="text-warning w-100">`+count_result+`/`+this.events.length+`</h3>
                                        <div class="progress">
                                              <div class="progress-bar position-relative" style="width: `+progress+`%">
                                                  <span class="ml-2">`+count_result+`/`+this.events.length+`</span>
                                              </div>
                                        </div>`;
        };
        Mbti.prototype.previousQuestion = function() {
            if(this.index > 0){
                this.index = this.index - 1;
                this.data = this.events[this.index];
                this.draw();
            }
        }
        Mbti.prototype.nextQuestion = function() {
            if(this.index < (this.events.length - 1)){
                this.index = (this.index + 1);
                this.data = this.events[this.index];
            }
            this.draw();
        }
        Mbti.prototype.showQuestion = function(index) {
            this.index = Number.parseInt(index);
            this.data = this.events[index];
            this.draw();
        }

        Mbti.prototype.chooseAnswer = function() {
            let self = this;
            let index = self.index;
            let question_id = self.data.id;
            let input = event.target;
            let answer = input.value;
            result[index] = answer;
            if(this.index < (this.events.length - 1)){
                this.index = this.index + 1;
                this.data = this.events[this.index];
                setTimeout(function(){
                    input.checked = false;
                    self.draw();
                }, 200);
            }else{
                setTimeout(function(){
                    self.draw();
                }, 200);
            }
            document.getElementById("question"+index).classList.add("active");
            if(Object.keys(result).length == this.events.length){
                sendResult();
            }
        }
        window.Mbti = Mbti;
        function createElement(tagName, className, innerText) {
            var ele = document.createElement(tagName);
            if(className) {
              ele.className = className;
            }
            if(innerText) {
              ele.innderText = ele.textContent = innerText;
            }
            return ele;
        }
        var answer_arr = {E:0,I:0,S:0,N:0,T:0,F:0,J:0,P:0};
        function sendResult(){
            Object.keys(result).forEach(function(key) {
                answer_arr[result[key]]++;
            });
            let mark = ["EI", "SN", "TF", "JP"];
            let res = "";

            mark.map((item) => {
              if (!answer_arr[item[0]]) answer_arr[item[0]] = 0;
              if (!answer_arr[item[1]]) answer_arr[item[1]] = 0;

              if (answer_arr[item[0]] > answer_arr[item[1]]) res += item[0];
              else res += item[1];
            });
            $.ajax({
                url:'/api/save-result',
                method:'POST',
                data: {answer:answer_arr,code:res,member_id:{!!$member_id!!}},
                success: function (response){
                   if(response.success == true){
                       window.location.href = '/home/result';
                   }
                }
            })
        }
        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
                    display.text(minutes + ":" + seconds);
                    if(display.html() === '00:01'){
                        sendResult();
                    }
                    if (--timer < 0) {
                            timer = duration;
                    }
            }, 1000);
        }

        jQuery(function ($) {
            var fiveMinutes = 15*60,
                    display = $('#time');
            startTimer(fiveMinutes, display);

        });
    }();
    !function() {
        var data = {!!$question_arr!!};
        var mbti = new Mbti('#mbti', data);
        document.querySelectorAll('.choose-question').forEach(item => {
            item.addEventListener('click', event => {
                mbti.showQuestion(item.getAttribute("data-index"));
            })
        })
    }();
</script>
@stop
