@extends('frontend.layouts.index')
@section('content')
<link href="{!!asset('frontend/css/sweetalert2.min.css')!!}" rel="stylesheet">
<div class="wrapper position-relative d-flex">
    <div class="wizard-content-1 clearfix pt-5 w-75 mx-auto">
        <div class="step-inner-content clearfix position-relative">
            <div class="form-area position-relative">
                <div class="multisteps-form__panel js-active" data-animation="fadeIn">
                    <div class="wizard-forms clearfix position-relative py-3 mx-3" id="mbti">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('script')
@parent
<script src="{!!asset('frontend/js/sweetalert2.min.js')!!}"></script>
    <script>
        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
                    display.text(minutes + ":" + seconds);
                    if(display.html() === '00:01'){
                        sendResult();
                    }
                    if (--timer < 0) {
                            timer = duration;
                    }
            }, 1000);
        }

        jQuery(function ($) {
            var fiveMinutes = 5*60,
                    display = $('#time');
            startTimer(fiveMinutes, display);

        });
    </script>
    <script>
        var option = `<option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>`;
        !function() {
            function Mbti(selector, events) {
              this.el = document.querySelector(selector);
              this.events = events;
              this.index = 0;
              this.data = this.events;
              this.draw();
            }
            Mbti.prototype.draw = function() {
                this.drawTitle();
                this.drawAnswer();
                this.drawFooter();
            }
            Mbti.prototype.drawTitle = function() {
                var self = this;
                let data = self.data;
                let index = self.index;
                if(!this.header) {
                    this.header = createElement('div','quiz-title text-center');
                    //
                    this.divtime = createElement('div','d-inline-block');
                    this.upperheader = createElement('p','countdown d-flex align-items-center justify-content-between mb-4');
                    this.time = createElement('span','d-flex align-items-center');
                    this.time.innerHTML = '<span class="d-flex align-items-center"><ion-icon name="alarm-outline" class="pr-2 text-danger clock"></ion-icon><span id="time">05:00</span></span>';
                    this.upperheader.appendChild(this.time);
                    //
                    this.content = createElement('p');
                    //Append the Elements
                    this.divtime.appendChild(this.upperheader);
                    this.header.appendChild(this.divtime);
                    this.header.appendChild(this.content);
                    this.el.appendChild(this.header);
                }
                this.content.innerHTML = "Khảo sát về các mức độ đánh giá";
            };
            Mbti.prototype.drawAnswer = function() {
                let self = this;
                let data = self.data;
                if(!this.answer) {
                    this.answer = createElement('div','quiz-option-selector pt-0 pb-4 row');
                    //
                    for(let i=0;i<data.length;i++){
                        this.sub_anwer = createElement('div','col-md-6 my-1');
                        this.answer.appendChild(this.sub_anwer);
                        this.sub_anwer.innerHTML = ` <label class="h-100 quest position-relative mt-2 py-2 px-2 d-flex rounded border border-muted bg-white">
                                                        <div class="number-input d-flex justify-content-center align-items-center">
                                                            <select class="quantity exp-option-box" name="quantity" id="q`+(i+1)+`">
                                                                `+option+`
                                                            </select>
                                                        </div>
                                                        <div class="my-auto pl-2">
                                                            <p>`+data[i]['question']+`.</p>
                                                        </div>
                                                    </label>`;
                    }
                    this.el.appendChild(this.answer);
                }
                document.querySelectorAll('button').forEach(item => {
                    item.addEventListener('click', event => {
                       self.chooseAnswer();
                    })
                })
            };
            Mbti.prototype.drawFooter = function() {
                let self = this;
                if(!this.footer){
                    this.footer = createElement('div','d-flex justify-content-end w-100 pt-2');
                    this.el.appendChild(this.footer);
                }
                this.footer.innerHTML =  `<h6 class="text-warning pt-2 pr-3 text-right d-flex align-items-center">
                                            <a class="pr-1 pl-1 text-white btn btn-key" href="javascript:void(0)" id="finish">Hoàn thành bài test</a>
                                        </h6>`;
            };

            Mbti.prototype.chooseAnswer = function() {
                let score = $('.score').html();
                if(score == 'Số điểm còn lại: 0'){
                    let self = this;
                    let index = self.index;
                    let question_id = self.data.id;
                    this.index = this.index + 1;
                    this.data = this.events[this.index];

                    document.getElementById("question"+index).classList.add("active");
                    if(Object.keys(result).length == this.events.length){
                        sendResult();
                    }
                    setTimeout(function(){
                        self.draw();
                    }, 200);
                }
                //
            }
            window.Mbti = Mbti;
            function createElement(tagName, className, innerText) {
                var ele = document.createElement(tagName);
                if(className) {
                  ele.className = className;
                }
                if(innerText) {
                  ele.innderText = ele.textContent = innerText;
                }
                return ele;
            }

        }();
        !function() {
            var data = {!!$question_arr!!};

            var mbti = new Mbti('#mbti', data);
            document.querySelectorAll('.choose-question').forEach(item => {
                    item.addEventListener('click', event => {
                       mbti.showQuestion(item.getAttribute("data-index"));
                    })
            })
        }();
        var result = {};
        function sendResult(){
            let i=1;
            var data = {!!$question_arr!!};
            $('select').each(function(){
                result['q'+i] = $('#q'+i).val();
                result['qid'+i] = data[i-1]['id'];

                i++;
            })
            result['member_id'] = {!!$member_id!!};
            result['type'] = data[0]['type'];
            --i;
            console.log(result);
            $.ajax({
                url:'/api/save-result-quick2',
                method:'POST',
                data: {result:result,index:i},
                success: function(response){
                    if(response.success == true){
                        Swal.fire({
                            title: 'Chúc mừng bạn đã hoàn thành bài test',
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Xác nhận'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = '/';
                            }
                        })
                    }
                }
            })
        }
        $('#finish').click(function(){
            sendResult();
        })


    </script>
@stop
