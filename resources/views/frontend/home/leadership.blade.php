@extends('frontend.layouts.index')
@section('content')
<link href="{!!asset('frontend/css/sweetalert2.min.css')!!}" rel="stylesheet">
<div class="wrapper position-relative d-flex">
    <div class="wrapper menu position-relative w-25">
        <h5 class="question-title position-absolute w-100 text-center pt-2 pb-2 border-bottom border-muted">Danh sách câu hỏi</h5>
        <div class="h-100 bg-white border-right border-warning py-5 pl-2">
            {{-- <h5 class="pt-2 mr-2">Thời gian còn lại: <span id="time" class="text-dark">05:00</span></h5> --}}
            <div class="pt-2 d-flex align-content-start flex-wrap ">
                @foreach($questions as $key=>$val)
                <div class="mr-1 mt-2 quest-item">
                    <a href="javascript:void(0)" class="choose-question" data-index='{{$key}}'>
                        <h6 class="border border-info rounded px-1 text-center py-2 bg-muted" id='question{{$key}}'>{{++$key}}</h6>
                    </a>
                </div>
                @endforeach
            </div>
            {{-- <h6 class="text-success pt-3 pb-3 pl-2">
                <a href="" class="d-flex">Hoàn thành bài thi <ion-icon name="checkmark-outline" class="pl-1"></ion-icon></a>
            </h6> --}}
        </div>
    </div>
    <div>
        <span class="menu-remote">
            <ion-icon name="reorder-four-outline" class="pl-2 pt-2"></ion-icon>
        </span>
    </div>
    <div class="wizard-content-1 clearfix mt-5 pb-5 w-75 mx-auto">
        <div class="steps d-inline-block position-absolute clearfix">
            <ul class="tablist multisteps-form__progress">
                <li class="multisteps-form__progress-btn js-active current"></li>
                <li class="multisteps-form__progress-btn "></li>
                <li class="multisteps-form__progress-btn"></li>
                <li class="multisteps-form__progress-btn"></li>
                <li class="multisteps-form__progress-btn"></li>
            </ul>
        </div>
        <div class="step-inner-content clearfix position-relative w-100">
            <form class="multisteps-form__form" action="https://jthemes.net/themes/html/BeWizard/Quiz/thank-you.html" id="wizard" method="POST">
                <div class="form-area position-relative">
                    <div class="multisteps-form__panel js-active" data-animation="fadeIn">
                        <div class="wizard-forms clearfix position-relative pt-0 pb-3 mx-2 rounded" id="mbti" >
                            {{-- <div class="quiz-title text-center">
                                <span>Câu hỏi 1</span>
                                <p>Tôi sẽ đóng vai là người phát ngôn của nhóm.</p>
                            </div> --}}
                            {{-- <div class="quiz-option-selector pt-2 pb-0 d-flex justify-content-center">
                                <label class="select-area quiz-details-option position-relative mt-2 mx-2 py-3 px-4">
                                    <input type="radio" name="quiz" value="L" class="exp-option-box">
                                    <p class="my-auto">L</p>
                                </label>
                                <label class="select-area quiz-details-option position-relative mt-2 mx-2 py-3 px-4">
                                    <input type="radio" name="quiz" value="K" class="exp-option-box">
                                    <p class="my-auto">K</p>
                                </label>
                                <label class="select-area quiz-details-option position-relative mt-2 mx-2 py-3 px-4">
                                    <input type="radio" name="quiz" value="T" class="exp-option-box">
                                    <p class="my-auto">T</p>
                                </label>
                                <label class="select-area quiz-details-option position-relative mt-2 mx-2 py-3 px-4">
                                    <input type="radio" name="quiz" value="H" class="exp-option-box">
                                    <p class="my-auto">H</p>
                                </label>
                                <label class="select-area quiz-details-option position-relative mt-2 mx-2 py-3 px-4">
                                    <input type="radio" name="quiz" value="O" class="exp-option-box">
                                    <p class="my-auto">O</p>
                                </label>
                            </div>
                            <div class="w-100 mt-5">
                                <h5 class="text-warning pb-3 pr-4 d-flex justify-content-end">
                                    <a href="" class="d-flex">
                                        Hoàn thành bài thi
                                        <ion-icon name="checkmark-outline" class="pl-1"></ion-icon>
                                    </a>
                                </h5>
                            </div>
                            <div class="actions mt-5 d-flex justify-content-between align-items-center mx-auto">
                                <div class="li ml-3 mt-1">
                                    <span class="js-btn-next" title="PREV"><ion-icon name="chevron-back-circle"></ion-icon></span>
                                </div>
                                <div class="quiz-progress-area w-75 d-flex align-items-center">
                                    <h3 class="text-warning w-100">5/10</h3>
                                    <div class="progress">
                                        <div class="progress-bar position-relative" style="width: 50%">
                                            <span class="ml-2">5/10</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="li mr-3 mt-1">
                                    <span class="js-btn-next" title="NEXT"><ion-icon name="chevron-forward-circle"></ion-icon></span>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@stop
@section('script')
@parent
<script src="{!!asset('frontend/js/sweetalert2.min.js')!!}"></script>
<script>
    !function() {
        function Mbti(selector, events) {
          this.el = document.querySelector(selector);
          this.events = events;
          this.index = 0;
          this.data = this.events[0];
          this.draw();
        }
        var result = {};
        Mbti.prototype.draw = function() {
            this.drawTitle();
            this.drawAnswer();
            this.drawFooter();
        }
        Mbti.prototype.drawTitle = function() {
            var self = this;
            let data = self.data;
            let index = self.index;
            if(!this.header) {
                this.header = createElement('div','quiz-title text-center');
                //
                this.upperheader = createElement('p','countdown d-flex align-items-center justify-content-between mb-4 pt-3');
                this.previous = createElement('div','li');
                this.previous.addEventListener('click', function() { self.previousQuestion(); });
                this.previous.innerHTML = '<a href="javascript:void(0)"><span class="d-flex align-items-center" title="PREV"><ion-icon name="arrow-back"></ion-icon>Câu trước</span></a>';
                this.time = createElement('span','d-flex align-items-center');
                this.time.innerHTML = '<span class="d-flex align-items-center"><ion-icon name="alarm-outline" class="pr-2 text-danger clock"></ion-icon><span id="time" class="text-dark">05:00</span></span>';
                this.next = createElement('div','li');
                this.next.addEventListener('click', function() { self.nextQuestion(); });
                this.next.innerHTML = '<a href="javascript:void(0)"><span class="d-flex align-items-center" title="NEXT">Câu sau<ion-icon name="arrow-forward"></ion-icon></span></a>';
                this.upperheader.appendChild(this.previous);
                this.upperheader.appendChild(this.time);
                this.upperheader.appendChild(this.next);
                //
                this.content = createElement('p');
                //Append the Elements
                this.header.appendChild(this.upperheader);
                this.header.appendChild(this.content);
                this.el.appendChild(this.header);
            }
            this.content.innerHTML = (index + 1) +". "+data.question;
        };
        Mbti.prototype.drawAnswer = function() {
            let self = this;
            let data = self.data;
            if(!this.answer) {
                //
                this.answer = createElement('div','quiz-option-selector pt-2 pb-0 d-flex flex-column align-items-center');
                //
                this.answer1 = createElement('label','quiz-details-option position-relative mt-1 py-3 pr-2 pl-5 w-75');
                this.answer2 = createElement('label','quiz-details-option position-relative mt-1 py-3 pr-2 pl-5 w-75');
                this.answer3 = createElement('label','quiz-details-option position-relative mt-1 py-3 pr-2 pl-5 w-75');
                this.answer4 = createElement('label','quiz-details-option position-relative mt-1 py-3 pr-2 pl-5 w-75');
                this.answer5 = createElement('label','quiz-details-option position-relative mt-1 py-3 pr-2 pl-5 w-75');
                //
                this.answer.appendChild(this.answer1);
                this.answer.appendChild(this.answer2);
                this.answer.appendChild(this.answer3);
                this.answer.appendChild(this.answer4);
                this.answer.appendChild(this.answer5);
                this.el.appendChild(this.answer);
            }
            if(result[this.index]){
                switch(result[this.index].answer) {
                    case 'L':
                        check_answer1 = 'checked';
                        check_answer2 = '';
                        check_answer3 = '';
                        check_answer4 = '';
                        check_answer5 = '';
                      break;
                    case 'K':
                        check_answer1 = '';
                        check_answer2 = 'checked';
                        check_answer3 = '';
                        check_answer4 = '';
                        check_answer5 = '';
                      break;
                    case 'T':
                      check_answer1 = '';
                      check_answer2 = '';
                      check_answer3 = 'checked';
                      check_answer4 = '';
                      check_answer5 = '';
                    break;
                    case 'H':
                      check_answer1 = '';
                      check_answer2 = '';
                      check_answer3 = '';
                      check_answer4 = 'checked';
                      check_answer5 = '';
                    break;
                    case 'O':
                      check_answer1 = '';
                      check_answer2 = '';
                      check_answer3 = '';
                      check_answer4 = '';
                      check_answer5 = 'checked';
                    break;
                
                }
            }else{
                check_answer1 = '';
                check_answer2 = '';
                check_answer3 = '';
                check_answer4 = '';
                check_answer5 = '';
            }
            this.answer1.innerHTML = ` <input type="radio" name="quiz" value="L" class="exp-option-box" `+check_answer1+`>
                                        <span class="select-area"></span>
                                        <span>Luôn luôn<span>`;
            this.answer2.innerHTML = ` <input type="radio" name="quiz" value="K" class="exp-option-box" `+check_answer2+`>
                                        <span class="select-area"></span>
                                        <span>Khá thường xuyên<span>`;
            this.answer3.innerHTML = ` <input type="radio" name="quiz" value="T" class="exp-option-box" `+check_answer3+`>
                                        <span class="select-area"></span>
                                        <span>Thỉnh thoảng<span>`;
            this.answer4.innerHTML = ` <input type="radio" name="quiz" value="H" class="exp-option-box" `+check_answer4+`>
                                        <span class="select-area"></span>
                                        <span>Hiếm khi<span>`;
            this.answer5.innerHTML = ` <input type="radio" name="quiz" value="O" class="exp-option-box" `+check_answer5+`>
                                        <span class="select-area"></span>
                                        <span>Không bao giờ<span>`;
            document.querySelectorAll('.exp-option-box').forEach(item => {
                item.addEventListener('change', event => {
                   self.chooseAnswer();
                })
            })
        };
        Mbti.prototype.drawFooter = function() {
            let self = this;
            if(!this.footer){
                this.footer = createElement('div','actions mt-5 d-flex justify-content-between align-items-center mx-5');
                this.progress = createElement('div','quiz-progress-area w-100 d-flex align-items-center');
                this.footer.appendChild(this.progress);
                this.el.appendChild(this.footer);
            }
            if(result){
                count_result = Object.keys(result).length;
            }else{
                count_result = 0;
            }
            let progress = (count_result / this.events.length)*100;
            this.progress.innerHTML =  `<h3 class="text-warning w-100">`+count_result+`/`+this.events.length+`</h3>
                                        <div class="progress">
                                              <div class="progress-bar position-relative" style="width: `+progress+`%">
                                                  <span class="ml-2">`+count_result+`/`+this.events.length+`</span>
                                              </div>
                                        </div>`;
        };
        Mbti.prototype.previousQuestion = function() {
            if(this.index > 0){
                this.index = this.index - 1;
                this.data = this.events[this.index];
                this.draw();
            }
        }
        Mbti.prototype.nextQuestion = function() {
            this.index = (this.index + 1);
            this.data = this.events[this.index];
            this.draw();
        }
        Mbti.prototype.showQuestion = function(index) {
            this.index = Number.parseInt(index);
            this.data = this.events[index];
            this.draw();
        }
        function sendResult(){
            let human_arr = [3,5,8,10,15,18,19,22,24,26,28,30,32,34,35];
            let answer_add = ['L','K'];
            let total_human = 0;
            let total_work = 0;
            Object.keys(result).forEach(function(key) {
               if(human_arr.includes(result[key].question_id)){
                   if(answer_add.includes(result[key].answer)){
                        total_human ++;
                   }
               }
               if(answer_add.includes(result[key].answer)){
                        total_work ++;
                }
            });
            total_work = total_work - total_human;
            $.ajax({
                url:'/api/save-result-leadership',
                method:'POST',
                data: {work:total_work,human:total_human,member_id:{!!$member_id!!}},
                success: function (response){
                   if(response.success == true){
                        window.location.href = '/home/result-leadership';     
                   }
                }
            })
        }
        Mbti.prototype.chooseAnswer = function() {
            let self = this;
            let index = self.index;
            let question_id = self.data.id;
            let input = event.target;
            let answer = input.value;
            result[index] = {'question_id':question_id,'answer':answer};
            if(this.index < (this.events.length - 1)){
                this.index = this.index + 1;
                this.data = this.events[this.index];
                setTimeout(function(){
                    input.checked = false;
                    self.draw();
                }, 200);
            }else{
                setTimeout(function(){
                    self.draw();
                }, 200);
            }
            document.getElementById("question"+index).classList.add("active");
            if(Object.keys(result).length == this.events.length){
                sendResult();
            }
        }
        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
                    display.text(minutes + ":" + seconds);
                    if(display.html() === '00:01'){
                       sendResult();
                    }
                    if (--timer < 0) {
                            timer = duration;
                    }
            }, 1000);
        }

        jQuery(function ($) {
            var fiveMinutes = 5*60,
                    display = $('#time');
            startTimer(fiveMinutes, display);

        });
        window.Mbti = Mbti;
        function createElement(tagName, className, innerText) {
            var ele = document.createElement(tagName);
            if(className) {
              ele.className = className;
            }
            if(innerText) {
              ele.innderText = ele.textContent = innerText;
            }
            return ele;
        }
    }();
    
    !function() {
        var data = {!!$question_arr!!};
        function addDate(ev) {

        }
        var mbti = new Mbti('#mbti', data);
        document.querySelectorAll('.choose-question').forEach(item => {
                item.addEventListener('click', event => {
                   mbti.showQuestion(item.getAttribute("data-index"));
                })
        })
    }();
</script>
@stop
