@extends('frontend.layouts.index')
@section('content')
<link rel="stylesheet" type="text/css" href="{!!asset('frontend/css/style-result.css')!!}"/>

<div class="container" id="result-page">
    <div id="congrat-modal-quick">
        <div class="row pl-5 ml-5">
            <a href="{{route('home.index')}}" class="position-absolute back-btn ml-5 mt-5" style="text-decoration: none; font-size: 18px">
                <span class="d-flex align-items-center mt-4 rounded px-2 py-1" title="NEXT" style="color: #fff; background: #ff7700;"><ion-icon name="home" class="pr-1"></ion-icon><span>Trang chủ</span></span>
            </a>
        </div>
        <div class="container h-100 d-flex align-items-center">
            <div class="col-md-4 bg-white mx-auto rounded d-flex align-items-center flex-column">
                <span class="text-center py-3">
                    <ion-icon name="checkmark-circle" style="font-size: 120px; color: rgb(106, 250, 40)"></ion-icon>
                </span>
                <p class="text-success text-center">Chúc mừng bạn đã hoàn thành bài thi.</p>
            </div>
        </div>
    </div>

@stop
