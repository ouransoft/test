@extends('frontend.layouts.index')
@section('content')

<div class="wrapper position-relative d-flex">
    <div class="wrapper menu position-relative w-25">
        <h5 class="question-title position-absolute w-100 text-center pt-2 pb-2 border-bottom border-muted">Danh sách câu hỏi</h5>
        <div class="h-100 bg-white border-right border-warning py-5 pl-2">
            <div class="pt-2 d-flex align-content-start flex-wrap ">
                @foreach($questions as $key=>$val)
                <div class="mr-1 mt-2 quest-item">
                    <a href="javascript:void(0)" class="choose-question" data-index='{{$key}}'>
                        <h6 class="border border-warning rounded px-1 text-center py-2 bg-muted" id='question{{$key}}'>{{++$key}}</h6>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div>
        <span class="menu-remote">
            <ion-icon name="reorder-four-outline" class="pl-2 pt-2"></ion-icon>
        </span>
    </div>
    <div class="wizard-content-1 clearfix pt-5 w-75 mx-auto">
        <div class="step-inner-content clearfix position-relative">
            <div class="form-area position-relative">
                <div class="multisteps-form__panel js-active" data-animation="fadeIn">
                    <div class="wizard-forms clearfix position-relative py-3 mx-3" id="mbti">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('script')
@parent
    <script>
        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                    minutes = parseInt(timer / 60, 10);
                    seconds = parseInt(timer % 60, 10);

                    minutes = minutes < 10 ? "0" + minutes : minutes;
                    seconds = seconds < 10 ? "0" + seconds : seconds;
                    display.text(minutes + ":" + seconds);
                    if(display.html() === '00:01'){
                        sendResult();
                    }
                    if (--timer < 0) {
                            timer = duration;
                    }
            }, 1000);
        }

        jQuery(function ($) {
            var fiveMinutes = 10*60,
                    display = $('#time');
            startTimer(fiveMinutes, display);

        });
    </script>
    <script>
        var option = `<option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>`;
        !function() {
            function Mbti(selector, events, events2) {
              this.el = document.querySelector(selector);
              this.events = events;
              this.events2 = events2;
              this.index = 0;
              this.num_ques = 'answer'+1;
              this.data = this.events[0];
            //   console.log(this.events2);
              this.data_answer = this.events2;
              this.draw();
            }
            var result = {};
            Mbti.prototype.draw = function() {
                this.drawTitle();
                this.drawAnswer();
                this.drawFooter();
            }
            Mbti.prototype.drawTitle = function() {
                var self = this;
                let data = self.data;
                let index = self.index;
                if(!this.header) {
                    this.header = createElement('div','quiz-title text-center');
                    //
                    this.upperheader = createElement('p','countdown d-flex align-items-center justify-content-between mb-4');
                    this.previous = createElement('div','li');
                    this.previous.addEventListener('click', function() { self.previousQuestion(); });
                    this.previous.innerHTML = '<a href="javascript:void(0)"><span class="d-flex align-items-center" title="PREV"><ion-icon name="chevron-back"></ion-icon>Quay lại</span></a>';
                    this.time = createElement('span','d-flex align-items-center');
                    this.time.innerHTML = '<span class="d-flex align-items-center"><ion-icon name="alarm-outline" class="pr-2 text-danger clock"></ion-icon><span id="time">05:00</span></span>';
                    this.next = createElement('div','li');
                    this.next.addEventListener('click', function() { self.nextQuestion(); });
                    this.next.innerHTML = '<a href="javascript:void(0)"><span class="d-flex align-items-center" title="NEXT">Tiếp theo<ion-icon name="chevron-forward"></ion-icon></span></a>';
                    this.upperheader.appendChild(this.previous);
                    this.upperheader.appendChild(this.time);
                    this.upperheader.appendChild(this.next);
                    //
                    this.content = createElement('p');
                    this.content2 = createElement('p','text-left score');
                    //Append the Elements
                    this.header.appendChild(this.upperheader);
                    this.header.appendChild(this.content);
                    this.header.appendChild(this.content2);
                    this.el.appendChild(this.header);
                }
                this.content.innerHTML = (index + 1) +". "+data.question;
                this.content2.innerHTML = 'Số điểm còn lại: 10';
            };
            Mbti.prototype.drawAnswer = function() {
                let self = this;
                let data_answer = self.data_answer;
                let num_ques = self.num_ques;
                if(!this.answer) {
                    this.answer = createElement('div','quiz-option-selector pt-0 pb-4 row');
                    //
                    this.answer1 = createElement('div','col-md-6 my-1');
                    this.answer2 = createElement('div','col-md-6 my-1');
                    this.answer3 = createElement('div','col-md-6 my-1');
                    this.answer4 = createElement('div','col-md-6 my-1');
                    this.answer5 = createElement('div','col-md-6 my-1');
                    this.answer6 = createElement('div','col-md-6 my-1');
                    this.answer7 = createElement('div','col-md-6 my-1');
                    this.answer8 = createElement('div','col-md-6 my-1');
                    //
                    this.answer.appendChild(this.answer1);
                    this.answer.appendChild(this.answer2);
                    this.answer.appendChild(this.answer3);
                    this.answer.appendChild(this.answer4);
                    this.answer.appendChild(this.answer5);
                    this.answer.appendChild(this.answer6);
                    this.answer.appendChild(this.answer7);
                    this.answer.appendChild(this.answer8);
                    this.el.appendChild(this.answer);
                }
                
                this.answer1.innerHTML = ` <label class="h-100 quest position-relative mt-2 py-2 px-2 d-flex rounded border border-muted bg-white">
                                                <div class="number-input d-flex justify-content-center align-items-center">
                                                    <select class="quantity exp-option-box" name="quantity" id="IM">
                                                        `+option+`
                                                    </select>
                                                </div>
                                                <div class="my-auto pl-2">
                                                    <p>`+data_answer[0][num_ques]+`.</p>
                                                </div>
                                            </label>`;
                this.answer2.innerHTML = ` <label class="h-100 quest position-relative mt-2 py-2 px-2 d-flex rounded border border-muted bg-white">
                                                <div class="number-input d-flex justify-content-center align-items-center">
                                                    <select class="quantity exp-option-box" name="quantity" id="CO">
                                                        `+option+`
                                                    </select>
                                                </div>
                                                <div class="my-auto pl-2">
                                                    <p>`+data_answer[1][num_ques]+`.</p>
                                                </div>
                                            </label>`;
                this.answer3.innerHTML = ` <label class="h-100 quest position-relative mt-2 py-2 px-2 d-flex rounded border border-muted bg-white">
                                                <div class="number-input d-flex justify-content-center align-items-center">
                                                    <select class="quantity exp-option-box" name="quantity" id="SH">
                                                        `+option+`
                                                    </select>
                                                </div>
                                                <div class="my-auto pl-2">
                                                    <p>`+data_answer[2][num_ques]+`.</p>
                                                </div>
                                            </label>`;
                this.answer4.innerHTML = ` <label class="h-100 quest position-relative mt-2 py-2 px-2 d-flex rounded border border-muted bg-white">
                                                <div class="number-input d-flex justify-content-center align-items-center">
                                                    <select class="quantity exp-option-box" name="quantity" id="PL">
                                                        `+option+`
                                                    </select>
                                                </div>
                                                <div class="my-auto pl-2">
                                                    <p>`+data_answer[3][num_ques]+`.</p>
                                                </div>
                                            </label>`;
                this.answer5.innerHTML = ` <label class="h-100 quest position-relative mt-2 py-2 px-2 d-flex rounded border border-muted bg-white">
                                                <div class="number-input d-flex justify-content-center align-items-center">
                                                    <select class="quantity exp-option-box" name="quantity" id="RI">
                                                        `+option+`
                                                    </select>
                                                </div>
                                                <div class="my-auto pl-2">
                                                    <p>`+data_answer[4][num_ques]+`.</p>
                                                </div>
                                            </label>`;
                this.answer6.innerHTML = ` <label class="h-100 quest position-relative mt-2 py-2 px-2 d-flex rounded border border-muted bg-white">
                                                <div class="number-input d-flex justify-content-center align-items-center">
                                                    <select class="quantity exp-option-box" name="quantity" id="ME">
                                                        `+option+`
                                                    </select>
                                                </div>
                                                <div class="my-auto pl-2">
                                                    <p>`+data_answer[5][num_ques]+`.</p>
                                                </div>
                                            </label>`;
                this.answer7.innerHTML = ` <label class="h-100 quest position-relative mt-2 py-2 px-2 d-flex rounded border border-muted bg-white">
                                                <div class="number-input d-flex justify-content-center align-items-center">
                                                    <select class="quantity exp-option-box" name="quantity" id="TW">
                                                        `+option+`
                                                    </select>
                                                </div>
                                                <div class="my-auto pl-2">
                                                    <p>`+data_answer[6][num_ques]+`.</p>
                                                </div>
                                            </label>`;
                this.answer8.innerHTML = `<label class="h-100 quest position-relative mt-2 py-2 px-2 d-flex rounded border border-muted bg-white">
                                                <div class="number-input d-flex justify-content-center align-items-center">
                                                    <select class="quantity exp-option-box" name="quantity" id="CF">
                                                        `+option+`
                                                    </select>
                                                </div>
                                                <div class="my-auto pl-2">
                                                    <p>`+data_answer[7][num_ques]+`.</p>
                                                </div>
                                            </label>`;
                checkScore();
                if(result[this.index]){
                    // console.log(result[this.index]['IM']);
                    $('#IM').val(parseInt(result[this.index]['IM']));
                    $('#CO').val(parseInt(result[this.index]['CO']));
                    $('#SH').val(parseInt(result[this.index]['SH']));
                    $('#PL').val(parseInt(result[this.index]['PL']));
                    $('#RI').val(parseInt(result[this.index]['RI']));
                    $('#ME').val(parseInt(result[this.index]['ME']));
                    $('#TW').val(parseInt(result[this.index]['TW']));
                    $('#CF').val(parseInt(result[this.index]['CF']));
                    this.content2.innerHTML = 'Số điểm còn lại: 0';
                }
                document.querySelectorAll('select').forEach(item => {
                    item.addEventListener('change', event => {
                       self.chooseAnswer();
                    })
                })
            };
            Mbti.prototype.drawFooter = function() {
                let self = this;
                if(!this.footer){
                    this.footer = createElement('div','actions mt-5 d-flex justify-content-between align-items-center mx-auto');
                    this.progress = createElement('div','quiz-progress-area w-100 d-flex align-items-center')
                    this.footer.appendChild(this.progress);
                    this.el.appendChild(this.footer);
                }
                if(result){
                    count_result = Object.keys(result).length;
                }else{
                    count_result = 0;
                }
                let progress = (count_result / this.events.length)*100;
                this.progress.innerHTML =  `<h3 class="text-warning w-100">`+count_result+`/`+this.events.length+`</h3>
                                            <div class="progress">
                                                  <div class="progress-bar position-relative" style="width: `+progress+`%">
                                                      <span class="ml-2">`+count_result+`/`+this.events.length+`</span>
                                                  </div>
                                            </div>`;
            };
            Mbti.prototype.previousQuestion = function() {
                if(this.index > 0){
                    this.index = this.index - 1;
                    this.num_ques = 'answer'+(this.index+1);
                    this.data = this.events[this.index];
                    this.data_answer = this.events2;
                    this.draw();
                    checkDisable();
                }
            }
            Mbti.prototype.nextQuestion = function() {

                if(this.index < (this.events.length - 1)){
                    this.index = (this.index + 1);
                    this.num_ques = 'answer'+(this.index+1);
                    this.data = this.events[this.index];
                    this.data_answer = this.events2;
                    this.draw();
                    checkDisable();
                }
            }
            Mbti.prototype.showQuestion = function(index) {
                this.index = Number.parseInt(index);
                this.data = this.events[index];
                this.num_ques = 'answer'+(this.index+1);
                this.data_answer = this.events2;
                this.draw();
                checkDisable();
            }

            Mbti.prototype.chooseAnswer = function() {
                let sum = 0;
                $('select').each(function(){
                    sum = sum + parseInt($(this).val());
                })
                if(sum > 0){
                    $('select').each(function(){
                         var current = 10 - sum;
                         var option_change = '';
                         if($(this).val() > current){
                             current = $(this).val();
                         }
                         console.log(current);
                         for(i=0;i <= current;i++){
                            if(parseInt($(this).val()) == i){
                                option_change += '<option value='+i+' selected>'+i+'</option>';
                            }else{
                                option_change += '<option value='+i+'>'+i+'</option>';
                            }
                        }
                        $(this).html(option_change);
                       
                    })
                }
                checkScore();
                let score = $('.score').html();
                if(sum == 10){
                    let self = this;
                    let index = self.index;
                    let num_ques = self.num_ques;
                    let question_id = self.data.id;
                    this.index = this.index + 1;
                    this.num_ques = 'answer'+(this.index+1);
                    this.data = this.events[this.index];
                    this.data_answer = this.events2;
                    result[index] = {
                                        'question_id':question_id,
                                        'IM':$('#IM').val(),
                                        'CO':$('#CO').val(),
                                        'SH':$('#SH').val(),
                                        'PL':$('#PL').val(),
                                        'RI':$('#RI').val(),
                                        'ME':$('#ME').val(),
                                        'TW':$('#TW').val(),
                                        'CF':$('#CF').val(),
                                    };

                    document.getElementById("question"+index).classList.add("active");
                    if(Object.keys(result).length == this.events.length){
                        sendResult();
                    }
                    setTimeout(function(){
                        self.draw();
                    }, 200);
                }
                //
            }
            window.Mbti = Mbti;
            function createElement(tagName, className, innerText) {
                var ele = document.createElement(tagName);
                if(className) {
                  ele.className = className;
                }
                if(innerText) {
                  ele.innderText = ele.textContent = innerText;
                }
                return ele;
            }
            function sendResult(){
                var sum_result = {'IM':0,'CO':0,'SH':0,'PL':0,'RI':0,'ME':0,'TW':0,'CF':0};
                for( var el in result ) {
                    if( result.hasOwnProperty( el ) ) {
                        sum_result['IM'] += parseFloat( result[el]['IM'] );
                        sum_result['CO'] += parseFloat( result[el]['CO'] );
                        sum_result['SH'] += parseFloat( result[el]['SH'] );
                        sum_result['PL'] += parseFloat( result[el]['PL'] );
                        sum_result['RI'] += parseFloat( result[el]['RI'] );
                        sum_result['ME'] += parseFloat( result[el]['ME'] );
                        sum_result['TW'] += parseFloat( result[el]['TW'] );
                        sum_result['CF'] += parseFloat( result[el]['CF'] );
                    }
                }
                max = "IM";
                for(var tmp in sum_result){
                    if(sum_result[tmp] > sum_result[max]){
                        max = tmp;
                    }
                }
                $.ajax({
                    url:'/api/save-result-teamwork',
                    method:'POST',
                    data: {result:sum_result, code:max,member_id:{!!$member_id!!}},
                    success: function (response){
                        if(response.success == true){
                            window.location.href = '/home/result-teamwork';
                        }
                    }
                })
            }
        }();
        !function() {
            var data = {!!$question_arr!!};
            var data_answer = {!!$answer_arr!!};
            function addDate(ev) {

            }
            var mbti = new Mbti('#mbti', data, data_answer);
            document.querySelectorAll('.choose-question').forEach(item => {
                    item.addEventListener('click', event => {
                       mbti.showQuestion(item.getAttribute("data-index"));
                    })
            })
        }();
        function checkScore(){
            $('body').delegate('select','change',function(){
                var sum = 0;
                $('select').each(function(){
                    sum += parseInt($(this).val());
                })
                $('.score').html("Số điểm còn lại: " + (10-sum));
                checkDisable();
            })
        }
        function checkDisable(){
            $('.score').html();
            if($('.score').html()=="Số điểm còn lại: 0"){
                $('.plus').attr("disabled", true);
            }else{
                $('.plus').attr("disabled", false);
            }
        }

    </script>
@stop
