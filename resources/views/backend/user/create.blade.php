@extends('backend.layouts.admin')
@section('content')
<section class="content-header">
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Thêm tài khoản</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{!!route('dashboard')!!}">Dashboard</a></li>
        <li class="breadcrumb-item active">Quản lý tài khoản</li>
      </ol>
    </div>
  </div>
</div><!-- /.container-fluid -->
</section>
<div class="card card-primary">
<!-- /.card-header -->
<!-- form start -->
<form action="{!!route('user.store')!!}" method="POST">
    @csrf
  <div class="card-body">
    <div class="form-group">
      <label for="exampleInputEmail1">Họ tên</label>
      <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Nhập tên quyền" required="">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Email</label>
      <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Nhập email" required="">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Mật khẩu</label>
      <input type="password" name="password" class="form-control" id="exampleInputEmail1" placeholder="Nhập mật khẩu" required="">
    </div>
    <div class="form-group">
      <label for="exampleInputEmail1">Xác nhận mật khẩu</label>
      <input type="password" name="confirm-password" class="form-control" id="exampleInputEmail1" placeholder="Nhập lại mật khẩu" required="">
    </div>
    <div class="form-group">
        <label>Quyền</label>
        <select class="form-control select2" name="roles[]" data-placeholder="Chọn quyền" style="width: 100%;" multiple="">
          {!!$roles_html!!}
        </select>
    </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Tạo mới</button>
  </div>
</form>
</div>
@stop
@section('script')
@parent
<script>
  $(function () {
    $('.select2').select2();
  });
</script>
@stop
