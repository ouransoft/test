@extends('backend.layouts.admin')
@section('content')
<section class="content-header">
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Cập nhật quyền</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{!!route('dashboard')!!}">Dashboard</a></li>
        <li class="breadcrumb-item active">Quản lý quyền</li>
      </ol>
    </div>
  </div>
</div><!-- /.container-fluid -->
</section>
<div class="card card-primary">
<!-- /.card-header -->
<!-- form start -->
<form action="{!!route('roles.update',$role->id)!!}" method="POST">
    @method('PUT')
    @csrf
  <div class="card-body">
    <div class="form-group">
      <label for="exampleInputEmail1">Tên quyền</label>
      <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Nhập tên quyền" required="" value='{!!$role->name!!}'>
    </div>
    <label>Danh sách quyền</label>
    <div class="form-group">
      @foreach($records as $key=>$record)
      <div class="custom-control custom-checkbox">
        <input class="custom-control-input" name='permission[]' type="checkbox" id="customCheckbox{!!++$key!!}" value="{!!$record->id!!}" @if(in_array($record->id, $rolePermissions)) checked @endif>
        <label for="customCheckbox{!!$key!!}" class="custom-control-label form-check-label">{!!$record->name!!}</label>
      </div>
      @endforeach
    </div>
  </div>
  <!-- /.card-body -->

  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>
</div>
@stop
