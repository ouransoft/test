@extends('backend.layouts.admin')
@section('content')
<section class="content-header">
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Thêm công ty</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{!!route('dashboard')!!}">Dashboard</a></li>
        <li class="breadcrumb-item active">Quản lý công ty</li>
      </ol>
    </div>
  </div>
</div><!-- /.container-fluid -->
</section>
<div class="card card-primary">
<!-- /.card-header -->
<!-- form start -->
<form action="{!!route('company.store')!!}" method="POST">
    @csrf
  <div class="card-body">
    <div class="form-group">
      <label for="company_name">Tên công ty</label>
      <input type="text" name="name" class="form-control" id="company_name" placeholder="Nhập tên công ty" required="">
    </div>
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Tạo mới</button>
  </div>
</form>
</div>
@stop
@section('script')
@parent
<script>
  $(function () {
    $('.select2').select2();
  });
</script>
@stop
