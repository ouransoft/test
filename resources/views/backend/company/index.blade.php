@extends('backend.layouts.admin')
@section('content')
<section class="content-header">
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Quản lý công ty</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{!!route('dashboard')!!}">Dashboard</a></li>
        <li class="breadcrumb-item active">Quản lý công ty</li>
      </ol>
    </div>
  </div>
</div><!-- /.container-fluid -->
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-header text-right">
                        <a class="btn btn-primary" href="{!!route('company.create')!!}"><i class="nav-icon fas fa-plus-circle text-white"></i> Thêm mới</a>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên công ty</th>
                                    <th>Mã code</th>
                                    <th>Ngày tạo</th>
                                    <th>Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($records as $key=>$record)
                                <tr>
                                    <td>{!! ++$key !!}</td>
                                    <td>{!! $record->name !!}</td>
                                    <td>{!! $record->code !!}</td>
                                    <td>{!! date('d/m/Y',strtotime($record->created_at)) !!}</td>
                                    <td class="d-flex ">
                                        <a class='btn text-blue px-1' href="{!!route('company.edit',$record->id)!!}"><i class="nav-icon fas fa-edit"></i></a>
                                        <form action="{{ route('company.destroy', $record->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn px-1">
                                               <i class="nav-icon fas fa-trash-alt text-red"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
@stop
@section('script')
@parent
<script>
  $(function () {
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@stop
