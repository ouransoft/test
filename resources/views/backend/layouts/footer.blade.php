<footer class="main-footer">
    <strong>Copyright &copy; 2021 <a href="https://ouransoft.vn">Ouransoft</a>.</strong>
    All rights reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<script src={{ asset('AdminLTE/plugins/jquery/jquery.min.js') }}></script>
<!-- jQuery UI 1.11.4 -->
<script src={{ asset('AdminLTE/plugins/jquery-ui/jquery-ui.min.js') }}></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)

</script>
<!-- Bootstrap 4 -->
<script src={{ asset('AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js') }}></script>
<!-- ChartJS -->
<script src={{ asset('AdminLTE/plugins/chart.js/Chart.min.js') }}></script>
<!-- JQVMap -->
<script src={{ asset('AdminLTE/plugins/jqvmap/jquery.vmap.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/jqvmap/maps/jquery.vmap.usa.js') }}></script>
<!-- jQuery Knob Chart -->
<script src={{ asset('AdminLTE/plugins/jquery-knob/jquery.knob.min.js') }}></script>
<!-- daterangepicker -->
<script src={{ asset('AdminLTE/plugins/moment/moment.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/daterangepicker/daterangepicker.js') }}></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src={{ asset('AdminLTE/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}>
</script>
<!-- overlayScrollbars -->
<script src={{ asset('AdminLTE/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}></script>
<!-- AdminLTE App -->
<script src={{ asset('AdminLTE/dist/js/adminlte.js') }}></script>
<!-- AdminLTE for demo purposes -->
<script src={{ asset('AdminLTE/dist/js/demo.js') }}></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src={{ asset('AdminLTE/plugins/select2/js/select2.full.min.js') }}></script>

<script src={{ asset('AdminLTE/plugins/moment/moment.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/inputmask/jquery.inputmask.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}></script>

{{-- tabledata --}}
<script src={{ asset('AdminLTE/plugins/datatables/jquery.dataTables.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/toastr/toastr.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/jszip/jszip.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/pdfmake/pdfmake.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/pdfmake/vfs_fonts.js') }}></script>
<script src={{ asset('AdminLTE/plugins/datatables-buttons/js/buttons.html5.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/datatables-buttons/js/buttons.print.min.js') }}></script>
<script src={{ asset('AdminLTE/plugins/datatables-buttons/js/buttons.colVis.min.js') }}></script>


{{-- customJS --}}

<script>
    $(function() {
        //Initialize Select2 Elements
        $('.select2').select2();

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    })

</script>
