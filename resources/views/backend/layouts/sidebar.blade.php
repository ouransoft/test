<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="https://ouransoft.vn" class="brand-link d-flex logo-admin">
        <img src="{!!asset('AdminLTE/img/logo.png')!!}" alt="Logo" class="brand-image">
    </a>
    <!-- Sidebar -->
    <div class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-transition os-host-scrollbar-horizontal-hidden"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px -8px; width: 249px; height: 204px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px 8px; height: 100%; width: 100%;">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="{!! Auth::user()->profile_photo_url !!}" class="img-circle elevation-2" alt="{!!Auth::user()->name!!}">
                        </div>
                        <div class="info">
                            <a href="{{route('profile.show')}}" class="d-block">{!!Auth::user()->name!!}</a>
                        </div>
                    </div>
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
                                 with font-awesome or any other icon font library -->
                            <li class="nav-item">
                                <a href="{!!route('dashboard')!!}" class="nav-link @if(Route::currentRouteName() == 'dashboard') active @endif">
                                    <i class="nav-icon fas fa-tachometer-alt"></i>
                                    <p>
                                        Dashboard
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{!!route('company.index')!!}" class="nav-link @if(Route::currentRouteName() == 'company.index') active @endif">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Quản lý công ty
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{!!route('member.index')!!}" class="nav-link @if(Route::currentRouteName() == 'member.index') active @endif">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Quản lý thành viên
                                    </p>
                                </a>
                            </li>
                            @can('roles-index')
                            <li class="nav-item">
                                <a href="{{route('user.index')}}" class="nav-link @if(Route::currentRouteName() == 'user.index') active @endif">
                                    <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Quản lý tài khoản
                                    </p>
                                </a>
                            </li>
                            @endcan
                            @can('roles-index')
                            <li class="nav-item">
                                <a href="{{route('roles.index')}}" class="nav-link @if(Route::currentRouteName() == 'roles.index') active @endif">
                                    <i class="nav-icon fas fa-user-shield"></i>
                                    <p>
                                        Quản lý phân quyền
                                    </p>
                                </a>
                            </li>
                            @endcan
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
            </div>
        </div>
        <div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable os-scrollbar-auto-hidden">
            <div class="os-scrollbar-track">
                <div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div>  
            </div>
        </div>
        <div class="os-scrollbar os-scrollbar-vertical os-scrollbar-auto-hidden">
            <div class="os-scrollbar-track">
                <div class="os-scrollbar-handle" style="height: 15.1069%; transform: translate(0px, 0px);">
                </div>      
            </div>  
        </div>
        <div class="os-scrollbar-corner">
        </div>
    </div>
    <!-- /.sidebar -->
</aside>
