@if (Session::get('success'))
<script>
    toastr.success('{{ Session::get('success') }}')
</script>
@endif

@if (Session::get('error'))
    <script>
        toastr.error('{{ Session::get('success') }}')
    </script>
@endif

@if (Session::get('warning'))
    <script>
        toastr.warning('{{ Session::get('success') }}')
    </script>
@endif

@if (Session::get('info'))
    <script>
        toastr.info('{{ Session::get('success') }}')
    </script>
@endif
