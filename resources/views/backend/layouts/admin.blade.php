<!DOCTYPE html>
<html lang="en">
@include('backend.layouts.head')
<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        @include('backend.layouts.header')
        <!-- /.navbar -->
        <!-- Main Sidebar Container -->
        @include('backend.layouts.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper pt-2">
            <!-- Content Header (Page header) -->
            @yield('contentHeader')
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- ./wrapper -->
    @include('backend.layouts.footer')
    @include('backend.layouts.mess')
    @yield('script')
</body>

</html>
