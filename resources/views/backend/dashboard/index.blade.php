@extends('backend.layouts.admin')
@section('content')
@if(!is_null(\Auth::user()->code))
<style>
    #container {
        height: 400px;
    }

    .highcharts-figure, .highcharts-data-table table {
      min-width: 310px;
      max-width: 800px;
      margin: 1em auto;
    }

    .highcharts-data-table table {
      font-family: Verdana, sans-serif;
      border-collapse: collapse;
      border: 1px solid #EBEBEB;
      margin: 10px auto;
      text-align: center;
      width: 100%;
      max-width: 500px;
    }
    .highcharts-data-table caption {
      padding: 1em 0;
      font-size: 1.2em;
      color: #555;
    }
    .highcharts-data-table th {
            font-weight: 600;
      padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
      padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
      background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
      background: #f1f7ff;
    }
</style>
@if(!is_null($companys))
<div class="d-flex ml-3 my-3">
    <h5 class="mb-0">Chọn công ty:</h5>
    <select class="bg-transparent data-select form-select ml-1 border-top-0 border-left-0 border-right-0" id="company_select" aria-label="Default select example">
        @if ($companys!=NULL)
            @foreach ($companys as $company)
                <option value="{{$company->id}}">{{$company->name}}</option>
            @endforeach
            @else
                <option selected>Không có dữ liệu</option>
        @endif
    </select>
</div>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Biểu đồ MBTI</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
      <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body" style="display: block;">
        <div class="form-group">
          <select class="select2bs4 form-control select-mbti" multiple style="wdth:50%" data-placeholder="Tìm kiếm thành viên">
              {!!$member_html!!}
          </select>
        </div>
        <div class="form-group">
           <a class="btn btn-primary export-mbti" href="javascript:void(0)">Xuất file</a>
        </div>
        <div class="mx-2 mb-3 mt-2">
            <h5 class="none-data-chart text-danger ml-3 mt-3" style="display:none">Không có dữ liệu</h5>
            <div id="chart"></div>
            <!-- <div class="note_mbti note-chart">
                <span><b>E</b>: Hướng ngoại</span>
                <span><b>S</b>: Giác quan</span>
                <span><b>T</b>: Lý trí</span>
                <span><b>J</b>: Nguyên tắc</span>
                <span><b>I</b>: Hướng nội</span>
                <span><b>N</b>: Trực giác</span>
                <span><b>F</b>: Cảm xúc</span>
                <span><b>P</b>: Linh hoạt</span>
            </div> -->
        </div>
    </div>
</div>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Biểu đồ leadership</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
      <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body" style="display: block;">
        <div class="form-group">
           <a class="btn btn-primary export-leadership" href="javascript:void(0)">Xuất file</a>
        </div>
        <div class="mx-2 mb-3 mt-2">
            <h5 class="none-data-chart2 text-danger ml-3 mt-3" style="display:none">Không có dữ liệu</h5>
            <figure class="highcharts-figure">
                <div id="container"></div>
            </figure>
        </div>
    </div>
</div>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Biểu đồ kỹ năng làm việc nhóm</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
      <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body" style="display: block;">
        <div class="form-group">
          <select class="select2bs4 form-control select-teamwork" multiple style="wdth:50%" data-placeholder="Tìm kiếm thành viên">
              {!!$member_html!!}
          </select>
        </div>
        <div class="form-group">
           <a class="btn btn-primary export-teamwork" href="javascript:void(0)">Xuất file</a>
        </div>
        <div class="mx-2 mb-3 mt-2">
            <h5 class="none-data-chart3 text-danger ml-3 mt-3" style="display:none">Không có dữ liệu</h5>
            <div id="chart_teamwork"></div>
            <!-- <div class="note_teamwork note-chart">
                <span><b>IM</b>: Thực thi</span>
                <span><b>IO</b>: Điều phối</span>
                <span><b>SH</b>: Khuôn phép</span>
                <span><b>PL</b>: Sáng tạo</span>
                <span><b>RI</b>: Khám phá</span>
                <span><b>ME</b>: Điều hành</span>
                <span><b>TW</b>: Đoàn kết</span>
                <span><b>TF</b>: Hoàn thiện</span>
            </div> -->
        </div>
    </div>
</div>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Bảng so sánh giá trị cá nhân của nhóm</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
      <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body" style="display: block;">
        <div class="form-group">
           <a class="btn btn-primary export-personal-value" href="javascript:void(0)">Xuất file</a>
        </div>
        <h5 class="none-data-table-personal-value text-danger ml-3 mt-3" style="display: none">Không có dữ liệu</h5>
        <h5 class="none-data text-danger ml-3 mt-3" style="display: none">Không có dữ liệu</h5>
        <div class="d-flex" id="table_personal_value">
            <div class="col-md-3 px-0">
                <table class="table-question-personal-value table table-striped table-bordered mx-0 my-0 w-100" style="width:auto; ">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th style="white-space: nowrap">Nội dung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($questions_personal_value as $key => $question)
                            <tr class="question-personal-value{{$key}}">
                                <td>{{++$key}}</td>
                                <td>{{$question->question}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-7 px-0 result-side" style="overflow-x: scroll;overflow-y: hidden">
                <table class="table-result-personal-value table table-striped table-bordered w-100 mx-0 my-0" style="width:auto; ">
                    <thead>
                        <tr>
                            @foreach ($members_table_personal_value as $record)
                                <th style="white-space: nowrap">{{$record->name}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($questions_personal_value as $key => $question)
                            <tr class="answer-personal-value{{$key}}">
                                @foreach ($question->answer as $record)
                                    <td>{{$record}}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 px-0">
                <table class="table-avg-personal-value table table-striped table-bordered w-100 mx-0" style="width:auto; ">
                    <thead>
                        <tr>
                            <th>Max</th>
                            <th>Min</th>
                            <th>AVE</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($answer_avg_personal_value as $key => $avg)
                            <tr class="avg-personal-value{{$key}}">
                                    <td>{{$avg['max']}}</td>
                                    <td>{{$avg['min']}}</td>
                                    <td>{{$avg['avg']}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Bảng so sánh mức độ quan trọng của nhóm</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
      </div>
      <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body" style="display: block;">
        <div class="form-group">
           <a class="btn btn-primary export-critical-level" href="javascript:void(0)">Xuất file</a>
        </div>
        <h5 class="none-data-table-critical-level text-danger ml-3 mt-3" style="display: none">Không có dữ liệu</h5>
        <h5 class="none-data text-danger ml-3 mt-3" style="display: none">Không có dữ liệu</h5>
        <div class="d-flex" id="table_critical_level">
            <div class="col-md-3 px-0">
                <table class="table-question-critical-level table table-striped table-bordered mx-0 my-0 w-100" style="width:auto; ">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th style="white-space: nowrap">Nội dung</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($questions_critical_level as $key => $question)
                            <tr class="question-critical-level{{$key}}">
                                <td>{{++$key}}</td>
                                <td>{{$question->question}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-7 px-0 result-side" style="overflow-x: scroll;overflow-y: hidden">
                <table class="table-result-critical-level table table-striped table-bordered w-100 mx-0 my-0" style="width:auto; ">
                    <thead>
                        <tr>
                            @foreach ($members_table_critical_level as $record)
                                <th style="white-space: nowrap">{{$record->name}}</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($questions_critical_level as $key => $question)
                            <tr class="answer-critical-level{{$key}}">
                                @foreach ($question->answer as $record)
                                    <td>{{$record}}</td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-2 px-0">
                <table class="table-avg-critical-level table table-striped table-bordered w-100 mx-0" style="width:auto; ">
                    <thead>
                        <tr>
                            <th>Max</th>
                            <th>Min</th>
                            <th>AVE</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($answer_avg_critical_level as $key => $avg)
                            <tr class="avg-critical-level{{$key}}">
                                <td>{{$avg['max']}}</td>
                                <td>{{$avg['min']}}</td>
                                <td>{{$avg['avg']}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endif
@endif
@stop
@section('script')
@parent
@if(!is_null($companys))
<script type="text/javascript" src="{!!asset('frontend/js/apexcharts.min.js')!!}"></script>
<script src="{!!asset('frontend/js/highcharts.js')!!}"></script>
<script src="{!!asset('frontend/js/exporting.js')!!}"></script>
<script src="{!!asset('frontend/js/export-data.js')!!}"></script>
<script src="{!!asset('frontend/js/accessibility.js')!!}"></script>
<script>
    $('document').ready(function(){
        if({{$check_table_personal_value}} == 0){
            $('.none-data-table-personal-value').css('display','block');
            $('#table_personal_value').attr('style', 'display: none !important');
        }
        if({{$check_table_critical_level}} == 0){
            $('.none-data-table-critical-level').css('display','block');
            $('#table_critical_level').attr('style', 'display: none !important');
        }
        if({!!$check_chart!!} == 0){
            $('.note_mbti').css('display','none');
            $('.none-data-chart').attr('style', 'display: block !important');
        }
        if({!!$check_chart2!!} == 0){
            $('.none-data-chart2').attr('style', 'display: block !important');
        }
        if({!!$check_chart3!!} == 0){
            $('.note_teamwork').css('display','none');
            $('.none-data-chart3').attr('style', 'display: block !important');
        }
        reSizeTablePersonalValue();
        reSizeTableCriticalLevel();
    })
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }

    //chart
    var options = {
        series: {!!$member_result_arr!!},
        chart: {
            height: '400px',
            type: 'radar',
            toolbar: {
                show: true,
                offsetX: 0,
                offsetY: 0,
                tools: {
                download: true,
                selection: true,
                zoom: true,
                zoomin: true,
                zoomout: true,
                pan: true,
                reset: true | '<img src="/static/icons/reset.png" width="20">',
                customIcons: []
                },
                export: {
                csv: {
                    filename: undefined,
                    columnDelimiter: ',',
                    headerCategory: 'category',
                    headerValue: 'value',
                    dateFormatter(timestamp) {
                    return new Date(timestamp).toDateString()
                    }
                },
                svg: {
                    filename: undefined,
                },
                png: {
                    filename: undefined,
                }
                },
                autoSelected: 'zoom' 
            },
        },
        xaxis: {
            categories: ['Hướng ngoại', 'Giác quan', 'Lý trí', 'Nguyên tắc', 'Hướng nội', 'Trực giác','Cảm xúc','Linh hoạt']
        },
        legend: {
            position: 'left',
        },
        fill: {
            opacity: 0,
            colors: ['#fff']
        }
    };

    var chart = new ApexCharts(document.querySelector("#chart"), options);
    chart.render();
    //
    //chart team work
    var options_teamwork = {
        series: {!!$teamwork_result!!},
        chart: {
            height: '400px',
            type: 'radar',
            toolbar: {
                show: true
            }
        },
        xaxis: {
            categories: ['Thực thi', 'Điều phối', 'Khuôn phép', 'Sáng tạo', 'Khám phá', 'Điều hành','Đoàn kết','Hoàn thiện']
        },
        legend: {
            position: 'left',
        },
        fill: {
            opacity: 0,
            colors: ['#fff']
        }
    };

    var chart_teamwork = new ApexCharts(document.querySelector("#chart_teamwork"), options_teamwork);
    chart_teamwork.render();
    //
    function reSizeTablePersonalValue(){
        var index = 0;
        var height_ans = 0;
        $('.table-question-personal-value tbody tr').each(function(){
            height_ans = $('.question-personal-value'+index).height();
            $('.answer-personal-value'+index).height(height_ans);
            $('.question-personal-value'+index).height(height_ans);
            $('.avg-personal-value'+index).height(height_ans);
            index++;
        })
    }
    function reSizeTableCriticalLevel(){
        var index = 0;
        var height_ans = 0;
        $('.table-question-critical-level tbody tr').each(function(){
            height_ans = $('.question-critical-level'+index).height();
            $('.answer-critical-level'+index).height(height_ans);
            $('.question-critical-level'+index).height(height_ans);
            $('.avg-critical-level'+index).height(height_ans);
            index++;
        })
    }
    //
    $('body').delegate('.data-select','change',function(){
        var value = $(this).val();
        $.ajax({
            method: 'POST',
            url: '/api/dashboard/company-select',
            data: {value:value},
            success: function(res){
                $('.select-mbti').html(res.member_html);
                $('.select-teamwork').html(res.member_html);
                $('.select2bs4').select2({
                    theme: 'bootstrap4'
                })
                if(res.members_table_personal_value == 0){
                    $('.none-data-table-personal-value').css('display','block');
                    $('#table_personal_value').attr('style', 'display: none !important');
                }else{
                    $('.none-data-table-personal-value').css('display','none');
                    $('#table_personal_value').attr('style', 'display: flex !important');
                    $('.table-result-personal-value thead').html(res.result_head_personal_value);
                    $('.table-result-personal-value tbody').html(res.result_body_personal_value);
                    $('.table-avg-personal-value tbody').html(res.result_avg_body_personal_value);
                    reSizeTablePersonalValue();
                }
                if(res.members_table_critical_level == 0){
                    $('.none-data-table-critical-level').css('display','block');
                    $('#table_critical_level').attr('style', 'display: none !important');
                }else{
                    $('.none-data-table-critical-level').css('display','none');
                    $('#table_critical_level').attr('style', 'display: flex !important');
                    $('.table-result-critical-level thead').html(res.result_head_critical_level);
                    $('.table-result-critical-level tbody').html(res.result_body_critical_level);
                    $('.table-avg-critical-level tbody').html(res.result_avg_body_critical_level);
                    reSizeTableCriticalLevel();
                }
                if(res.check_chart1 == 0){
                    $('.none-data-chart').css('display','block');
                    $('.note_mbti').css('display','none');
                }else{
                    $('.none-data-chart').css('display','none');
                    $('.note_mbti').css('display','grid');
                }
                if(res.check_chart2 == 0){
                    $('.none-data-chart2').css('display','block');
                }else{
                    $('.none-data-chart2').css('display','none');
                }
                if(res.check_chart3 == 0){
                    $('.note_teamwork').css('display','none');
                    $('.none-data-chart3').css('display','block');
                }else{
                    $('.note_teamwork').css('display','grid');
                    $('.none-data-chart3').css('display','none');
                }
                chart.updateSeries(res.member_result);
                chart_teamwork.updateSeries(res.teamwork_result);
                chart_leadership.update({
                    xAxis: [{
                        categories: res.name_arr,
                      }],
                    series: [{
                        name: 'Phong cách lãnh đạo con người',
                        color: '#f47620',
                        displayNegative: true,
                        negativeColor: '#f47620'  ,
                        data:
                           res.human_arr

                      }, {
                        name: 'Phong cách lãnh đạo công việc',
                        color: 'blue',
                        displayNegative: true,
                        negativeColor: 'blue'  ,
                        data:
                          res.work_arr

                    }]
                });
            }
        })
    })
    var categories = {!!$name_arr!!};

   var chart_leadership =  Highcharts.chart('container', {
      chart: {
        type: 'bar',
      },

      title: {
        text: 'Biểu đồ thống kê phong cách lãnh đạo'
      },
      subtitle: {
        text: ''
      },
      accessibility: {
        point: {
          valueDescriptionFormat: '{index}. Age {xDescription}, {value}%.'
        }
      },
      xAxis: [{
        categories: categories,
        reversed: false,
        labels: {
          step: 1,
          style:{
              fontSize:"12px"
          }
        },
        accessibility: {
          description: 'Age (male)'
        }
      }],
      yAxis: {
        title: {
          text: null
        },
        labels: {
          formatter: function () {
            return Math.abs(this.value);
          }
        },
        accessibility: {
          description: 'Percentage population',
          rangeDescription: 'Range: 0 to 5%'
        },
        max:20
      },

      plotOptions: {
        series: {
           stacking: 'normal',
           dataLabels: {
                enabled: false
            }
        },
        columnrange: {
                dataLabels: {
                    enabled: true,
                    grouping:true,
                    formatter: function () {
                    if(this.y == 0)
                    	return "";
                    else
                        return this.y;
                    }
                }
        }
      },

      tooltip: {
        formatter: function () {
          return '<b>' + this.series.name + ' ' + this.point.category + '</b><br/>' +
            'Điểm: ' + Highcharts.numberFormat(Math.abs(this.point.y), 1);
        }
      },

      series: [{
        name: 'Phong cách lãnh đạo con người',
        color: '#f47620',
        displayNegative: true,
        negativeColor: '#f47620',
        data: [
           {!!implode(',',$human_arr)!!}
        ]
      }, {

        name: 'Phong cách lãnh đạo công việc',
        color: 'blue',
        displayNegative: true,
        negativeColor: 'blue',
        data: [
          {!!implode(',',$work_arr)!!}
        ]
      }]
    });
    $('body').delegate('.select-mbti','change',function(){
        let member_id = $(this).val();
        let company_id = $('#company_select').val();
        $.ajax({
            url:'/api/dashboard/search-chart-mbti',
            method:'POST',
            data:{member_id:member_id,company_id:company_id},
            success:function(data){
                chart.updateSeries(data.member_result);
            }
        })
    })
    $('body').delegate('.select-teamwork','change',function(){
        let member_id = $(this).val();
        let company_id = $('#company_select').val();
        $.ajax({
            url:'/api/dashboard/search-chart-teamwork',
            method:'POST',
            data:{member_id:member_id,company_id:company_id},
            success:function(data){
                chart_teamwork.updateSeries(data.teamwork_result);
            }
        })
    })
    $('.export-mbti').click(function(){
        let member_id = $('.select-mbti').val();
        let company_id = $('#company_select').val();
        $.ajax({
            url:'/api/dashboard/export-mbti',
            method:'POST',
            data:{member_id:member_id,company_id:company_id},
            success:function(data){
                window.location.href = window.location.protocol + "//" + window.location.host + "/"+data.link;
            }
        })
    })
    $('.export-teamwork').click(function(){
        let member_id = $('.select-teamwork').val();
        let company_id = $('#company_select').val();
        $.ajax({
            url:'/api/dashboard/export-teamwork',
            method:'POST',
            data:{member_id:member_id,company_id:company_id},
            success:function(data){
                window.location.href = window.location.protocol + "//" + window.location.host + "/"+data.link;
            }
        })
    })
    $('.export-leadership').click(function(){
        let company_id = $('#company_select').val();
        $.ajax({
            url:'/api/dashboard/export-leadership',
            method:'POST',
            data:{company_id:company_id},
            success:function(data){
                window.location.href = window.location.protocol + "//" + window.location.host + "/"+data.link;
            }
        })
    })
    $('.export-personal-value').click(function(){
        let company_id = $('#company_select').val();
        $.ajax({
            url:'/api/dashboard/export-personal-value',
            method:'POST',
            data:{company_id:company_id},
            success:function(data){
                window.location.href = window.location.protocol + "//" + window.location.host + "/"+data.link;
            }
        })
    })
    $('.export-critical-level').click(function(){
        let company_id = $('#company_select').val();
        $.ajax({
            url:'/api/dashboard/export-critical-level',
            method:'POST',
            data:{company_id:company_id},
            success:function(data){
                window.location.href = window.location.protocol + "//" + window.location.host + "/"+data.link;
            }
        })
    })
</script>
@endif
@stop
