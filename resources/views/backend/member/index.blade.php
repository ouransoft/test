@extends('backend.layouts.admin')
@section('content')
<section class="content-header">
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1>Quản lý thành viên</h1>
    </div>
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{!!route('dashboard')!!}">Dashboard</a></li>
        <li class="breadcrumb-item active">Quản lý thành viên</li>
      </ol>
    </div>
  </div>
</div><!-- /.container-fluid -->
</section>
<div class="card card-default">
    <div class="card-header">
      <h3 class="card-title">Tìm kiếm</h3>
      <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse">
          <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove">
          <i class="fas fa-times"></i>
        </button>
      </div>
    </div>
<!-- /.card-header -->
    <div class="card-body" style="display: block;">
      <div class="row">
        <div class="col-md-6">
            <form action="{{route('member.index')}}" method="GET" id="frmSearch">
                <div class="form-group">
                  <label>Tìm kiếm theo công ty</label>
                  <select class="form-control select2 select2-hidden-accessible select-company" name="company_id" style="width: 100%;" aria-hidden="true" data-placeholder="Chọn công ty">
                      {!!$company_html!!}
                  </select>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Họ tên</th>
                                    <th>Email</th>
                                    <th>Số điện thoại</th>
                                    <th>Công ty</th>
                                    <th>Ngày tạo</th>
                                    <th>Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($records as $key=>$record)
                                <tr>
                                    <td>{!! ++$key !!}</td>
                                    <td>{!! $record->name !!}</td>
                                    <td>{!! $record->email !!}</td>
                                    <td>{!! $record->phone !!}</td>
                                    <td>{!! $record->company ? $record->company->name : '' !!}</td>
                                    <td>{!! date('d/m/Y',strtotime($record->created_at)) !!}</td>
                                    <td class="d-flex ">
                                        <form action="{{ route('member.destroy', $record->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn px-1">
                                               <i class="nav-icon fas fa-trash-alt text-red"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
@stop
@section('script')
@parent
<script>
  $(function () {
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
  $('.select-company').change(function(){
      $('#frmSearch').submit();
  })
</script>
@stop
