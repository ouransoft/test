<div>
    <p><b>Hi {{ $name }},</b></p>
    <p>You received this email because you requested to recovery password at {{date('H:i A')}}, {{date('d/m/Y')}}</p>
    <p>Account Email: {{$email}}</p>
    <p>Click following link:</p>
    <p><a href="{{$href}}">{{$href}}</a></p>
    
    <p style="text-align: right; "><b><span style="font-family: Helvetica;"><span
                    style="font-family: Arial;">OURAN</span>
                <font color="#ff7700"><span style="font-family: Arial;">SOFT </span></font><span
                    style="font-family: Arial;">TECHNOLOGY JSC.</span>
            </span></b></p>
    <p style="text-align: right;font-size: 11px; ">No.3,Land 16th ext,Trung Hanh 5,Dang Lam Ward,Hai An,HaiPhong city |
        <font color="#ff7700">
            <b>Head</b>
        </font>
    </p>
    <p style="text-align: right;font-size: 11px ">No.2,16th Residential group,Thanh To Ward,Hai An Dist, HaiPhong city |
        <font color="#ff7700"> <b>Office</b></font>
    </p>
    <p style="text-align: right;font-size: 11px; ">
        <font color="#ff7700"><b>SMARTER,SIMPLER THAN EVER</b></font>
    </p>
    <p><br></p>
    <p><b><br></b></p>
</div>
