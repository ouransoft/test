<?php

namespace App\Helpers;
use DateTime;
class StringHelper {
    
    public static function getSelectHtml($options, $selected = '') {
        $html = '<option></option>';
        foreach ($options as $option) {
            $html .= '<option value="' . $option['id'] . '"' . ((is_array($selected) ? in_array($option['id'], $selected) : $selected == $option['id']) ? 'selected' : '') . '>' . $option['name'] . '</option>';
        }
        return $html;
    }
    public static function getSelectOptions($options, $selected = '') {
        $html = '<option></option>';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->id . '"' . ((is_array($selected) ? in_array($option->id, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->name . '</option>';
        }
        return $html;
    }
    public static function getSelectRolesOptions($options, $selected = '') {
        $html = '<option></option>';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->name . '"' . ((is_array($selected) ? in_array($option->name, $selected) : $selected == $option->name) ? 'selected' : '') . '>' . $option->name . '</option>';
        }
        return $html;
    }
    public static function getSelectOptionsNormal($options, $selected = '') {
        $html = '';
        foreach ($options as $option) {
            $html .= '<option value="' . $option->id . '"' . ((is_array($selected) ? in_array($option->id, $selected) : $selected == $option->id) ? 'selected' : '') . '>' . $option->title . '</option>';
        }
        return $html;
    }

    public static function ago($start_time,$end_time)
    {
        $dt = $end_time->diff($start_time);
        if ($dt->y > 0){
            $number = $dt->y;
            $unit = "Year";
        } else if ($dt->m > 0) {
            $number = $dt->m;
            $unit = "Month";
        } else if ($dt->d > 0) {
            $number = $dt->d;
            $unit = "Day";
        } else if ($dt->h > 0) {
            $number = $dt->h;
            $unit = "Hour";
        } else if ($dt->i > 0) {
            $number = $dt->i;
            $unit = "Minute";
        } else if ($dt->s > 0) {
            $number = $dt->s;
            $unit = "Second";
        }else{
            $number = 'now';
            $unit = "";
        }
        $ret = $number." ".$unit;
        return $ret;
    }
    public static function ConvertSizeFile($bytes){
        $bytes = floatval($bytes);
            $arBytes = array(
                0 => array(
                    "UNIT" => "TB",
                    "VALUE" => pow(1024, 4)
                ),
                1 => array(
                    "UNIT" => "GB",
                    "VALUE" => pow(1024, 3)
                ),
                2 => array(
                    "UNIT" => "MB",
                    "VALUE" => pow(1024, 2)
                ),
                3 => array(
                    "UNIT" => "KB",
                    "VALUE" => 1024
                ),
                4 => array(
                    "UNIT" => "B",
                    "VALUE" => 1
                ),
            );

        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }

}
