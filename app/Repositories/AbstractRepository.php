<?php

namespace App\Repositories;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;
use Validator;

abstract class AbstractRepository {

    private $app;
    protected $model;

    public function __construct(App $app) {
        $this->app = $app;
        $this->makeModel();
    }
    
    abstract function model();

    public function makeModel() {
        $model = $this->app->make($this->model());
        if (!$model instanceof Model) {
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }
        return $this->model = $model;
    }

    public function find($id)
    {
        try {
            return $this->model->findOrFail($id);
        } catch (Exception $ex) {
        }
        return false;
    }
    public function all()
    {

        return $this->model->all();
    }
    public function delete($id)
    {
        $res = $this->model->find($id);
        return $res->delete($id);
    }
    public function update($id, array $item)
    {
        $res = $this->model->find($id);
        return $res->update($item);
    }
    public function create(array $item)
    {
        return $this->model->create($item);
    }
    public function paginate($number)
    {
        return $this->model->paginate($number);
    }
    public function where($col, $value)
    {
        return $this->model->where($col, $value);
    }
}
