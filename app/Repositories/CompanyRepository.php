<?php
namespace App\Repositories;
use App\Repositories\AbstractRepository;

class CompanyRepository extends AbstractRepository
{
    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Models\Company';
    }
}
