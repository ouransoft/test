<?php
namespace App\Repositories;
use App\Repositories\AbstractRepository;

class UserRepository extends AbstractRepository
{
    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Models\User';
    }
    public function validateCreate(){
        return $rules = [
            'name' => 'unique:users',
        ];
    }
}
