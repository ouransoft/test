<?php
namespace App\Repositories;
use App\Repositories\AbstractRepository;

class MemberRepository extends AbstractRepository
{
    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Models\Member';
    }
    public function getByIds($member_ids){
        return $this->model->whereIn('id',$member_ids)->get();
    }
}
