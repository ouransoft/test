<?php
namespace App\Repositories;
use App\Repositories\AbstractRepository;

class HistoryQuickRepository extends AbstractRepository
{
    public function __construct(\Illuminate\Container\Container $app) {
        parent::__construct($app);
    }

    public function model() {
        return 'App\Models\HistoryTestQuick';
    }
    public function getDataTable($company_id,$type){
        $questions = \App\Models\QuestionQuick::where('type','=',$type)->get();
        $member_ids = \App\Models\HistoryTestQuick::groupBy('member_id')->where('type',$type)->pluck('member_id')->toArray();
        $members_table = \App\Models\Member::whereIn('id',$member_ids)->where('company_id','=',$company_id)->get();
        $answer_avg = array();
        foreach($questions as $key => $question){
            $answer_arr = array();
            $max = 0;
            $min = 9;
            foreach($members_table as $member){
                $history = \App\Models\HistoryTestQuick::where('question_id','=',$question->id)->where('type',$type)->where('member_id','=',$member->id)->first();
                if(is_null($history)){
                    $answer_arr[] = 0;
                }else{
                    if($history->answer > $max){
                        $max = $history->answer;
                    }
                    if($history->answer < $min){
                        $min = $history->answer;
                    }
                    $answer_arr[] = $history->answer;
                }
            }
            $avg = ($max+$min)/2;
            $answer_avg[$key]['max'] = $max;
            $answer_avg[$key]['min'] = $min;
            $answer_avg[$key]['avg'] = $avg;
            $question->answer = $answer_arr;
        }
        return ['member_table'=>$members_table,'questions'=>$questions,'answer_avg'=>$answer_avg];
    }
    public function deleteHistory($member_id,$type){
        return $this->model->where('member_id',$member_id)->where('type',$type)->delete();
    }
}
