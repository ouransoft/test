<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable
{
    use Notifiable;
    protected $table = 'member';
    protected $fillable = [
        'name',
        'email',
        'phone',
        'company_id',
        'gender',
        'password',
        'forgot_password'
    ];
    public function company() {
        return $this->belongsTo('\App\Models\Company');
    }
    public function histotyMbti(){
        return $this->hasMany('\App\Models\HistoryTest');
    }
    public function histotyTeamWork(){
        return $this->hasMany('\App\Models\HistoryTestTeamwork');
    }
    public function histotyLeadership(){
        return $this->hasMany('\App\Models\ChartLeadership');
    }
}
