<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionQuick extends Model
{
    use HasFactory;
    protected $table = 'question_quick';
    protected $fillable = [
        'order',
        'type',
        'question'
    ];
}
