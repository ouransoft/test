<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionTeamwork extends Model
{
    use HasFactory;
    protected $table = 'question_teamwork';
    protected $fillable = [
        'question',
    ];
}
