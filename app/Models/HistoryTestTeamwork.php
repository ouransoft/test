<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryTestTeamwork extends Model
{
    use HasFactory;
    protected $table = 'history_test_teamwork';
    protected $fillable = ['member_id','personality_id','chart_id'];
}
