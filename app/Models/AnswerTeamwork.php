<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnswerTeamwork extends Model
{
    use HasFactory;
    protected $table = 'answer_teamwork';
    protected $fillable = ['type','answer1','answer2','answer3','answer4','answer5','answer6','answer7'];
}
