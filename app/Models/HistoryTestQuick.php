<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryTestQuick extends Model
{
    use HasFactory;
    protected $table = 'history_test_quick';
    protected $fillable = ['member_id','type','question_id','answer'];
    const TYPE_PERSONAL_VALUE = 1;
    const TYPE_CRITICAL_LEVEL = 2;
}
