<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personality extends Model
{
    use HasFactory;
    protected $table = 'personality';
    protected $fillable = ['personality_id','member_id','chart_id'];
}
