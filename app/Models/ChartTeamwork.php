<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChartTeamwork extends Model
{
    use HasFactory;
    protected $table = 'chart_teamwork';
    protected $fillable = ['IM','CO','SH','PL','RI','ME','TW','CF'];
}
