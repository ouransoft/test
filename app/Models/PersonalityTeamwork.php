<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PersonalityTeamwork extends Model
{
    use HasFactory;
    protected $table = 'personality_teamwork';
    protected $fillable = ['personality_teamwork_id','member_id','chart_teamwork_id'];
}
