<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionLeadership extends Model
{
    use HasFactory;
    protected $table = 'question_leadership';
    protected $fillable = [
        'question'
    ];
}
