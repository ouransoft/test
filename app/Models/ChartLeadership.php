<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChartLeadership extends Model
{
    use HasFactory;
    protected $table = 'chart_leadership';
    protected $fillable = ['member_id','human','work'];
}
