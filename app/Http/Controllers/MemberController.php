<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\MemberRepository;

class MemberController extends Controller
{
    public function __construct(MemberRepository $memberRepo) {
        $this->memberRepo = $memberRepo;
    }
    
    public function index(Request $request){
        $company_arr = \Auth::user()->company->pluck('id')->toArray();
        $companys = \App\Models\Company::whereIn('id',$company_arr)->get();
        if($request->get('company_id') && in_array($request->get('company_id'),$company_arr)){
            $company_ids = [$request->get('company_id')];
            $company_html = \App\Helpers\StringHelper::getSelectOptions($companys,$request->get('company_id'));
        }else{
            $company_ids = $company_arr;
            $company_html = \App\Helpers\StringHelper::getSelectOptions($companys);
        }
        $records = \App\Models\Member::whereIn('company_id',$company_ids)->get();
        return view('backend.member.index',compact('records','company_html'));
    }
    public function destroy($id){
        $member = $this->memberRepo->find($id);
        $member->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
    }
}
