<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use App\Repositories\UserRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\MemberRepository;
use Mail;
class FrontendController extends Controller
{
    public function __construct(UserRepository $userRepo,CompanyRepository $companyRepo,MemberRepository $memberRepo) {
        $this->userRepo = $userRepo;
        $this->companyRepo = $companyRepo;
        $this->memberRepo = $memberRepo;
    }
    public function index(){
        return view('frontend.home.index');
    }
    private function randomQuestion($len, $matrix, $type)
        {
            $res = [];
            $mark = [];
            while (count($res) < $len) {
                $key = rand(0, $len - 1);
                if (!isset($mark[$key])) {
                    array_push($res, $matrix[$type][$key]);
                    $mark[$key] = true;
                }
            }

            return $res;
        }
    public function mbti(){
        $matrix = [];
        $question = \App\Models\Question::all();
        foreach ($question as $item) {
            if (!isset($matrix[$item->type])) {
                $matrix[$item->type] = [];
            }
            array_push($matrix[$item->type], $item);
        }
        $EI = $this->randomQuestion(10, $matrix, 'EI');
        $SN = $this->randomQuestion(20, $matrix, 'SN');
        $TF = $this->randomQuestion(20, $matrix, 'TF');
        $JP = $this->randomQuestion(20, $matrix, 'JP');
        $questions = [];
        $questions =  array_merge($EI, $SN, $TF, $JP);
        for ($i = 0; $i < count($questions); $i++) {
            $key = rand(0, count($questions) - 1);
            $tm = $questions[$i];
            $questions[$i] = $questions[$key];
            $questions[$key] = $tm;
        }
        $question_arr = json_encode($questions);
        $member_id = \Auth::guard('member')->user()->id;
        return view('frontend.home.mbti', compact('question_arr','questions','member_id'));
    }
    public function resultMbti(){
        $history = \App\Models\HistoryTest::where('member_id',\Auth::guard('member')->user()->id)->orderBy('created_at','DESC')->first();
        $personality = \App\Models\Personality::find($history->personality_id);
        $chart = \App\Models\Chart::find($history->chart_id)->toArray();
        $dataChart = [];
        $str = "ESTJINFP";
        for ($i = 0; $i < 8; $i++) {
          $dataChart[$i] = !$chart[$str[$i]] ? 0 : $chart[$str[$i]];
          //   dataChart[i] = ~~(dataChart[i] / 4);
          if ($str[$i] === "E" || $str[$i] === "I") {
            $dataChart[$i] = number_format(($dataChart[$i] / 2), 2, '.', "");
          } else $dataChart[$i] = number_format(($dataChart[$i] / 4), 2, '.', "");
        }
        return view('frontend.home.result',compact('personality','dataChart'));
    }
    public function resultLeadership(){
        $history = \App\Models\ChartLeadership::where('member_id',\Auth::guard('member')->user()->id)->orderBy('created_at','DESC')->first();
        return view('frontend.home.result-leadership',compact('history'));
    }
    public function leadership(){
        $questions = \App\Models\QuestionLeadership::all();
        $question_arr = json_encode($questions);
        $member_id = \Auth::guard('member')->user()->id;
        return view('frontend.home.leadership', compact('question_arr','questions','member_id'));
    }
    //
    public function teamwork(){
        $questions = \App\Models\QuestionTeamwork::all();
        $answers = \App\Models\AnswerTeamwork::all();
        $question_arr = json_encode($questions);
        $answer_arr = json_encode($answers);
        $member_id = \Auth::guard('member')->user()->id;

        return view('frontend.home.teamwork', compact('question_arr','answer_arr','questions','answers','member_id'));
    }
    public function resultTeamwork(){
        $history = \App\Models\HistoryTestTeamwork::where('member_id',\Auth::guard('member')->user()->id)->orderBy('created_at','DESC')->first();
        $personality = \App\Models\PersonalityTeamwork::find($history->personality_id);
        $chart = \App\Models\ChartTeamwork::find($history->chart_id)->toArray();
        $dataChart[0] = $chart['IM'];
        $dataChart[1] = $chart['CO'];
        $dataChart[2] = $chart['SH'];
        $dataChart[3] = $chart['PL'];
        $dataChart[4] = $chart['RI'];
        $dataChart[5] = $chart['ME'];
        $dataChart[6] = $chart['TW'];
        $dataChart[7] = $chart['CF'];
        return view('frontend.home.result-teamwork',compact('personality','dataChart'));
    }
    //
    public function quick(){
        $questions = \App\Models\QuestionQuick::where('type','=','1')->get();
        $question_arr = json_encode($questions);
        $member_id = \Auth::guard('member')->user()->id;

        return view('frontend.home.quick', compact('question_arr','questions','member_id'));
    }
    public function quick2(){
        $questions = \App\Models\QuestionQuick::where('type','=','2')->get();
        $question_arr = json_encode($questions);
        $member_id = \Auth::guard('member')->user()->id;
        return view('frontend.home.quick-2', compact('question_arr','questions','member_id'));
    }
    public function resultQuick(){
        return view('frontend.home.result-quick');
    }
    public function forgotPassword(Request $request){
        $email = $request->get('email');
        $member = \App\Models\Member::where('email',$email)->orderBy('updated_at','DESC')->first();
        $member->forgot_password = md5(time());
        $member->save();
        $href = \URL::to("/forgot-password/?email=".$email."&code=".$member->forgot_password);
        Mail::send('forgot-password', array('href' => $href,'name' => $member->name,'email' => $email), function($message) use ($email){
            $message->from('ouransoft@gmail.com', 'Ouransoft');
            $message->to($email, 'Ouransoft')->subject('Reset Password Information from Ouransoft');
        });
        return response()->json(array('success'=>true));
    }
    public function postforgotPassword(Request $request){
        $email = $request->get('email');
        $code = $request->get('amp;code');
        $password = $request->get('password');
        $member = \App\Models\Member::where('forgot_password',$code)->where('email',$email)->first();
        $member->password = bcrypt($password);
        $member->forgot_password = NULL;
        $member->save();
        return response()->json(array('success'=>true));
    }
}
