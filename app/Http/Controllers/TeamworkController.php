<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeamworkController extends Controller
{
    public function saveResult(Request $request){
        $input = $request->all();
        $chart_teamwork = \App\Models\ChartTeamwork::create($input['result']);
        $personality = \App\Models\PersonalityTeamwork::where('code',$input['code'])->first();
        $history_teamwork['chart_id'] = $chart_teamwork->id;
        $history_teamwork['personality_id'] = $personality->id;
        $history_teamwork['member_id'] = $input['member_id'];
        \App\Models\HistoryTestTeamwork::create($history_teamwork);
        return response()->json(array('success'=>true));
    }
}
