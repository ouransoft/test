<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\MemberRepository;
use Illuminate\Support\Facades\Redirect;
use App\Repositories\CompanyRepository;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct(MemberRepository $memberRepo,CompanyRepository $companyRepo) {
        $this->memberRepo = $memberRepo;
        $this->companyRepo = $companyRepo;
    }
    public function getRegister(){
        return view('frontend.auth.register');
    }
    public function postRegister(Request $request){
        $input = $request->all();
        $company = $this->companyRepo->where('code',$input['code'])->first();
        $check_email = \App\Models\Member::where('email',$input['email'])->first();
        if($check_email){
            return redirect()->route('home.get-register')
                        ->with('info','Email đã được sử dụng đăng ký');
        }
        if($company){
            $input['company_id'] = $company->id;
            $input['password'] = Hash::make($input['password']);
            $member = \App\Models\Member::create($input);
            \Auth::guard('member')->login($member);
            return redirect()->route('home.index');
        }else{
            return redirect()->route('home.get-register')
                        ->with('info','Mã công ty không tồn tại');
        }
    }
    public function logout(){
        \Auth::guard('member')->logout();
        return Redirect::route('home.get-login');
    }
    public function getLogin(){
        return view('frontend.auth.login');
    }
    public function forgotPassword(Request $request){
        $email = $request->email;
        $code = $request->code;
        $member = \App\Models\Member::where('forgot_password',$code)->where('email',$email)->first();
        if($member){
            return view('frontend.auth.forgot-password');
        }else{
            abort(404);
        }
    }
    public function postLogin(Request $request){
        $input = [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];
        if (\Auth::guard('member')->attempt($input)) {
            $member = \Auth::guard('member')->user();
            $member->save();
            return Redirect::route('home.index');
        }
        return Redirect::route('home.get-login')->with('info', 'Email hoặc password không đúng');
    }
}
