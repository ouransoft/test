<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LeadershipController extends Controller
{
    public function saveResult(Request $request){
        $input = $request->all();
        \App\Models\ChartLeadership::create($input);
        if($input['work'] > $input['human']){
            $message ='Bạn thuộc nhóm phong cách lãnh đạo theo công việc';
        }else{
            $message = 'Bạn thuộc nhóm phong cách lãnh đạo theo con người';
        }
        return response()->json(array('success'=>true,'message'=>$message));
    }
}
