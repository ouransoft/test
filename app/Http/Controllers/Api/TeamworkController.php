<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TeamworkController extends Controller
{
    public function saveResult(Request $request){
        $input = $request->all();
        $chart = \App\Models\ChartTeamwork::create($input['result']);
        $personality = \App\Models\PersonalityTeamwork::where('code',$input['code'])->first();
        $history['chart_id'] = $chart->id;
        $history['personality_id'] = $personality->id;
        $history['member_id'] = $input['member_id'];
        \App\Models\HistoryTestTeamwork::create($history);
        return response()->json(array('success'=>true));
    }
}
