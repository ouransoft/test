<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\HistoryQuickRepository;

class QuickController extends Controller
{
    public function __construct(HistoryQuickRepository $historytestquickRepo){
        $this->historytestquickRepo = $historytestquickRepo;
    }
    public function saveResultImportant(Request $request){
        $input = $request->all();
        $this->historytestquickRepo->deleteHistory($input["result"]["member_id"],$input["result"]["type"]);
        for($i=0;$i<$input['index'];$i++){
            $history['member_id'] = $input["result"]["member_id"];
            $history['question_id'] = $input["result"]["qid".($i+1)];
            $history['answer'] = $input["result"]["q".($i+1)];
            $history['type'] = $input["result"]["type"];
            \App\Models\HistoryTestQuick::create($history);
        }
        return response()->json(array('success'=>true));
    }
}
