<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;

class MbtiController extends Controller
{
    public function saveResult(Request $request){
        $input = $request->all();
        $chart = \App\Models\Chart::create($input['answer']);
        $personality = \App\Models\Personality::where('code',$input['code'])->first();
        $history['chart_id'] = $chart->id;
        $history['personality_id'] = $personality->id;
        $history['member_id'] = $input['member_id'];
        $member = \App\Models\Member::find($input['member_id']);
        $email = $member->email;
        \App\Models\HistoryTest::create($history);
        Mail::send('mail', array('overall' => strip_tags($personality->description),'name' => $member->name,'code' => $personality->code), function($message) use ($email){
                            $message->from('ouransoft@gmail.com', 'Ouransoft');
                            $message->to($email, 'Ouransoft')->subject('Thông báo kết quả bài test MBTI');
                        });
        return response()->json(array('success'=>true));
    }

}
