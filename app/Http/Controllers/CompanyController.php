<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;

class CompanyController extends Controller
{
    public function __construct(CompanyRepository $companyRepo) {
        $this->companyRepo = $companyRepo;
    }
    public function index(){
        $records = $this->companyRepo->where('user_id',\Auth::user()->id)->get();
        return view('backend.company.index',compact('records'));
    }
    public function create(){
        return view('backend.company.create');
    }
    public function store(Request $request)
    {
        $input = $request->all();
        $input['user_id'] = \Auth::user()->id;
        $input['code'] = time();
        $company = $this->companyRepo->create($input);
        return redirect()->route('company.index')
                        ->with('success','Tạo mới công ty thành công');
    }
    public function edit($id)
    {
        $record = $this->companyRepo->find($id);
        return view('backend.company.edit',compact('record'));
    }
    
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $company = $this->companyRepo->update($id,$input);
        return redirect()->route('company.index')
                        ->with('success','Cập nhật công ty thành công');
    }
    public function destroy($id){
        $company = $this->companyRepo->find($id);
        $company->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
    }
}
