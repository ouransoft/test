<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use function PHPUnit\Framework\isNull;
use App\Repositories\MemberRepository;
use App\Repositories\CompanyRepository;
use App\Repositories\HistoryQuickRepository;

class DashboardController extends Controller
{
    public function __construct(MemberRepository $memberRepo,CompanyRepository $companyRepo,HistoryQuickRepository $historyquickRepo){
        $this->memberRepo = $memberRepo;
        $this->companyRepo = $companyRepo;
        $this->historyquickRepo = $historyquickRepo;
    }

    public function index(){
        $user_id = \Auth::user()->id;
        $companys = \App\Models\Company::where('user_id','=',$user_id)->get();
        if($companys->isEmpty()){
            $companys = NULL;
            $member_result_arr = "";
            return view('backend.dashboard.index', compact('companys', 'member_result_arr'));
        }
        $companys = \App\Models\Company::where('user_id','=',$user_id)->get();
        $members = \App\Models\Member::where('company_id','=',$companys[0]->id)->get();
        $member_html = \App\Helpers\StringHelper::getSelectOptions($members);
        //MBTI
        $member_result = array();
        $answer = "ESTJINFP";
        foreach($members as $key => $record){
            $history = \App\Models\HistoryTest::where('member_id','=',$record->id)->first();
            $val = $key;
            if($history != null){
                $chart = \App\Models\Chart::where('id','=',$history->chart_id)->first()->toArray();
                for ($i = 0; $i < 8; $i++) {
                  $data[$i] = !$chart[$answer[$i]] ? 0 : $chart[$answer[$i]];
                  //   dataChart[i] = ~~(dataChart[i] / 4);
                  if ($answer[$i] === "E" || $answer[$i] === "I") {
                   $data[$i] = number_format(($chart[$answer[$i]] / 2), 2, '.', "");
                  } else $data[$i] = number_format(($chart[$answer[$i]] / 4), 2, '.', "");
                }
                $object = new \stdClass();
                $object->data = $data;
                $object->name = $record->name;;
                $member_result[] = $object;
            }
        }
        $check_chart = count($member_result);
        $member_result_arr = json_encode($member_result);
         //Table personal value
        $personal_value = $this->historyquickRepo->getDataTable($companys[0]->id,\App\Models\HistoryTestQuick::TYPE_PERSONAL_VALUE);
        $members_table_personal_value = $personal_value['member_table'];
        $questions_personal_value = $personal_value['questions'];
        $answer_avg_personal_value = $personal_value['answer_avg'];
        $check_table_personal_value = count($members_table_personal_value);
         //Table critical level
        $critical_level =  $this->historyquickRepo->getDataTable($companys[0]->id,\App\Models\HistoryTestQuick::TYPE_CRITICAL_LEVEL);
        $members_table_critical_level = $critical_level['member_table'];
        $questions_critical_level = $critical_level['questions'];
        $answer_avg_critical_level = $critical_level['answer_avg'];
        $check_table_critical_level = count($members_table_critical_level);
        //Leadership
        $name_arr = [];
        $human_arr = [];
        $work_arr = [];
        foreach($members as $key => $member){
            $chart_leadership = \App\Models\ChartLeadership::where('member_id','=',$member->id)->first();
            if($chart_leadership != null){
                $name_arr[] = $member->name;
                $human_arr[] = - $chart_leadership->human;
                $work_arr[] = $chart_leadership->work;
            }
        }
        $check_chart2 = count($name_arr);
        $name_arr = json_encode($name_arr);
        //Team work
        $teamwork_result_arr = array();
        $str = ['IM','CO','SH','PL','RI','ME','TW','CF'];
        foreach($members as $key => $record){
            $history = \App\Models\HistoryTestTeamwork::where('member_id','=',$record->id)->first();
            $val = $key;
            if($history != null){
                $chart = \App\Models\ChartTeamwork::where('id','=',$history->chart_id)->first()->toArray();
                for ($i = 0; $i < 8; $i++) {
                  $data[$i] = $chart[$str[$i]];
                }
                $object = new \stdClass();
                $object->data = $data;
                $object->name = $record->name;;
                $teamwork_result_arr[] = $object;
            }
        }
        $teamwork_result = json_encode($teamwork_result_arr);
        $check_chart3 = count($teamwork_result_arr);
        return view('backend.dashboard.index', compact('member_html','teamwork_result','companys', 'member_result_arr', 'check_chart', 'check_chart2','check_chart3',
                                                       'name_arr','human_arr','work_arr','members','members_table_personal_value','questions_personal_value',
                                                       'answer_avg_personal_value','check_table_personal_value','members_table_critical_level','questions_critical_level',
                                                       'check_table_critical_level','answer_avg_critical_level'));
    }

    public function companySelectChange(Request $request){
        $input = $request->all();
        $companys = \App\Models\Company::where('id','=',$input['value'])->get();
        $members = \App\Models\Member::where('company_id','=',$companys[0]->id)->get();
        $member_html = \App\Helpers\StringHelper::getSelectOptions($members);
        //MBTI
        $member_result = array();
        $answer = "ESTJINFP";
        foreach($members as $key => $record){
            $history = \App\Models\HistoryTest::where('member_id','=',$record->id)->first();
            $val = $key;
            if($history != null){
                $chart = \App\Models\Chart::where('id','=',$history->chart_id)->first()->toArray();
                for ($i = 0; $i < 8; $i++) {
                  $data[$i] = !$chart[$answer[$i]] ? 0 : $chart[$answer[$i]];
                  //   dataChart[i] = ~~(dataChart[i] / 4);
                  if ($answer[$i] === "E" || $answer[$i] === "I") {
                   $data[$i] = number_format(($chart[$answer[$i]] / 2), 2, '.', "");
                  } else $data[$i] = number_format(($chart[$answer[$i]] / 4), 2, '.', "");
                }
                $object = new \stdClass();
                $object->data = $data;
                $object->name = $record->name;;
                $member_result[] = $object;
            }
        }
        //Table quick
        //Table personal value
        $personal_value = $this->historyquickRepo->getDataTable($companys[0]->id,\App\Models\HistoryTestQuick::TYPE_PERSONAL_VALUE);
        $members_table_personal_value = $personal_value['member_table'];
        $questions_personal_value = $personal_value['questions'];
        $answer_avg_personal_value = $personal_value['answer_avg'];
        $check_table_personal_value = count($members_table_personal_value);
        $html_head_personal_value = "";
        $html_body_personal_value  = "";
        $html_avg_body_personal_value = "";
        foreach ($members_table_personal_value  as $record){
            $html_head_personal_value  .= "<th style='white-space: nowrap'>".$record['name']."</th>";
        }
        foreach ($questions_personal_value  as $key => $question){
            $score_html = "";
            foreach($question->answer as $record){
                $score_html .= "<td>".$record."</td>";

            }
            $html_body_personal_value  .= "<tr class='answer-personal-value".$key."'>"
                            .$score_html.
                        "</tr>";
        }
        foreach ($answer_avg_personal_value  as $key => $avg){
            $html_avg_body_personal_value .= "<tr class='avg-personal-value".$key."'>
                                                <td>".$avg['max']."</td>
                                                <td>".$avg['min']."</td>
                                                <td>".$avg['avg']."</td>
                                              </tr> ";
        }
         //Table critical level
        $critical_level =  $this->historyquickRepo->getDataTable($companys[0]->id,\App\Models\HistoryTestQuick::TYPE_CRITICAL_LEVEL);
        $members_table_critical_level = $critical_level['member_table'];
        $questions_critical_level = $critical_level['questions'];
        $answer_avg_critical_level = $critical_level['answer_avg'];
        $check_table_critical_level = count($members_table_critical_level);
        $html_head_critical_level = "";
        $html_body_critical_level  = "";
        $html_avg_body_critical_level = "";
        foreach ($members_table_critical_level  as $record){
            $html_head_critical_level  .= "<th style='white-space: nowrap'>".$record['name']."</th>";
        }
        foreach ($questions_critical_level  as $key => $question){
            $score_html = "";
            foreach($question->answer as $record){
                $score_html .= "<td>".$record."</td>";

            }
            $html_body_critical_level  .= "<tr class='answer-critical-level".$key."'>"
                            .$score_html.
                        "</tr>";
        }
        foreach ($answer_avg_critical_level  as $key => $avg){
            $html_avg_body_critical_level .= "<tr class='avg-critical-level".$key."'>
                                                <td>".$avg['max']."</td>
                                                <td>".$avg['min']."</td>
                                                <td>".$avg['avg']."</td>
                                              </tr> ";
        }

        //Leadership
        $name_arr = [];
        $human_arr = [];
        $work_arr = [];
        foreach($members as $key => $member){
            $chart_leadership = \App\Models\ChartLeadership::where('member_id','=',$member->id)->first();
            if($chart_leadership != null){
                $name_arr[] = $member->name;
                $human_arr[] = - $chart_leadership->human;
                $work_arr[] = $chart_leadership->work;
            }
        }
        $check_chart2 = count($name_arr);
       //Team work
        $teamwork_result_arr = array();
        $str = ['IM','CO','SH','PL','RI','ME','TW','CF'];
        foreach($members as $key => $record){
            $history = \App\Models\HistoryTestTeamwork::where('member_id','=',$record->id)->first();
            $val = $key;
            if($history != null){
                $chart = \App\Models\ChartTeamwork::where('id','=',$history->chart_id)->first()->toArray();
                for ($i = 0; $i < 8; $i++) {
                  $data[$i] = $chart[$str[$i]];
                }
                $object = new \stdClass();
                $object->data = $data;
                $object->name = $record->name;;
                $teamwork_result_arr[] = $object;
            }
        }
        return response()->json(['member_html'=>$member_html,'teamwork_result'=>$teamwork_result_arr,'check_chart3'=>count($teamwork_result_arr),'member_result'=>$member_result,
                                'check_chart1'=>count($member_result),'check_chart2'=>$check_chart2,'name_arr'=>$name_arr,'human_arr'=>$human_arr,'work_arr'=>$work_arr,
                                'result_head_personal_value'=>$html_head_personal_value, 'result_body_personal_value'=>$html_body_personal_value, 'result_avg_body_personal_value'=>$html_avg_body_personal_value, 'members_table_personal_value'=>count($members_table_personal_value),
                                'result_head_critical_level'=>$html_head_critical_level, 'result_body_critical_level'=>$html_body_critical_level, 'result_avg_body_critical_level'=>$html_avg_body_critical_level, 'members_table_critical_level'=>count($members_table_critical_level)
                                ]);
    }
    public function searchChartMbti(Request $request){
        $member_ids = $request->get('member_id');
        $company_id = $request->get('company_id');
        $company = $this->companyRepo->find($company_id);
        if($member_ids){
            $members = $this->memberRepo->getByIds($member_ids);
        }else{
            $members = \App\Models\Member::where('company_id','=',$company->id)->get();
        }
        $member_result = array();
        $answer = "ESTJINFP";
        foreach($members as $key => $record){
            $history = \App\Models\HistoryTest::where('member_id','=',$record->id)->first();
            $val = $key;
            if($history != null){
                $chart = \App\Models\Chart::where('id','=',$history->chart_id)->first()->toArray();
                for ($i = 0; $i < 8; $i++) {
                  $data[$i] = !$chart[$answer[$i]] ? 0 : $chart[$answer[$i]];
                  //   dataChart[i] = ~~(dataChart[i] / 4);
                  if ($answer[$i] === "E" || $answer[$i] === "I") {
                   $data[$i] = number_format(($chart[$answer[$i]] / 2), 2, '.', "");
                  } else $data[$i] = number_format(($chart[$answer[$i]] / 4), 2, '.', "");
                }
                $object = new \stdClass();
                $object->data = $data;
                $object->name = $record->name;;
                $member_result[] = $object;
            }
        }
        return response()->json(['member_result'=>$member_result]);
    }
    public function searchChartTeamwork(Request $request){
        $member_ids = $request->get('member_id');
        $company_id = $request->get('company_id');
        $company = $this->companyRepo->find($company_id);
        if($member_ids){
            $members = $this->memberRepo->getByIds($member_ids);
        }else{
            $members = \App\Models\Member::where('company_id','=',$company->id)->get();
        }
        $teamwork_result_arr = array();
        $str = ['IM','CO','SH','PL','RI','ME','TW','CF'];
        foreach($members as $key => $record){
            $history = \App\Models\HistoryTestTeamwork::where('member_id','=',$record->id)->first();
            $val = $key;
            if($history != null){
                $chart = \App\Models\ChartTeamwork::where('id','=',$history->chart_id)->first()->toArray();
                for ($i = 0; $i < 8; $i++) {
                  $data[$i] = $chart[$str[$i]];
                }
                $object = new \stdClass();
                $object->data = $data;
                $object->name = $record->name;;
                $teamwork_result_arr[] = $object;
            }
        }
        return response()->json(['teamwork_result'=>$teamwork_result_arr]);
    }
    public function exportMbti(Request $request){
        $member_ids = $request->get('member_id');
        $company_id = $request->get('company_id');
        $company = $this->companyRepo->find($company_id);
        if($member_ids){
            $members = $this->memberRepo->getByIds($member_ids);
        }else{
            $members = \App\Models\Member::where('company_id','=',$company->id)->get();
        }
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('B1', 'Hướng ngoại');
        $sheet->setCellValue('C1', 'Giác quan');
        $sheet->setCellValue('D1', 'Lý trí');
        $sheet->setCellValue('E1', 'Nguyên tắc');
        $sheet->setCellValue('F1', 'Hướng nội');
        $sheet->setCellValue('G1', 'Trực giác');
        $sheet->setCellValue('H1', 'Cảm xác');
        $sheet->setCellValue('I1', 'Linh hoạt');
        $rows = 2;
        $answer = "ESTJINFP";
        $member_result = [];
        foreach($members as $key => $record){
            $history = \App\Models\HistoryTest::where('member_id','=',$record->id)->first();
            $val = $key;
            if($history != null){
                $chart = \App\Models\Chart::where('id','=',$history->chart_id)->first()->toArray();
                for ($i = 0; $i < 8; $i++) {
                  $data[$i] = !$chart[$answer[$i]] ? 0 : $chart[$answer[$i]];
                  //   dataChart[i] = ~~(dataChart[i] / 4);
                  if ($answer[$i] === "E" || $answer[$i] === "I") {
                   $data[$i] = number_format(($chart[$answer[$i]] / 2), 2, '.', "");
                  } else $data[$i] = number_format(($chart[$answer[$i]] / 4), 2, '.', "");
                }
                $object = new \stdClass();
                $object->data = $data;
                $object->name = $record->name;;
                $member_result[] = $object;
            }
        }
        $colums = 'BCDEFGHI';
        foreach($member_result as $value){
            $sheet->setCellValue('A' . $rows, $value->name);
            foreach($value->data as $key=>$val){
                $sheet->setCellValue($colums[$key] . $rows, $val);
            }
            $rows++;
        }
        for ($i = 'B'; $i <=  $spreadsheet->getActiveSheet()->getHighestColumn() ; $i++) {
            $spreadsheet->getActiveSheet()->getColumnDimension($i)->setWidth(20);
        }
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $fileName = "Tinh_cach_ca_nhan.xlsx";
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save("export/".$fileName);
        header("Content-Type: application/vnd.ms-excel");
        return response()->json(array('link'=>'export/'.$fileName));
    }
    public function exportTeamwork(Request $request){
        $member_ids = $request->get('member_id');
        $company_id = $request->get('company_id');
        $company = $this->companyRepo->find($company_id);
        if($member_ids){
            $members = $this->memberRepo->getByIds($member_ids);
        }else{
            $members = \App\Models\Member::where('company_id','=',$company->id)->get();
        }
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('B1', 'Thực thi');
        $sheet->setCellValue('C1', 'Điều phối');
        $sheet->setCellValue('D1', 'Khuôn phép');
        $sheet->setCellValue('E1', 'Sáng tạo');
        $sheet->setCellValue('F1', 'Khám phá');
        $sheet->setCellValue('G1', 'Điều hành');
        $sheet->setCellValue('H1', 'Đoàn kết');
        $sheet->setCellValue('I1', 'Hoàn thiện');
        $rows = 2;
        $teamwork_result_arr = array();
        $str = ['IM','CO','SH','PL','RI','ME','TW','CF'];
        foreach($members as $key => $record){
            $history = \App\Models\HistoryTestTeamwork::where('member_id','=',$record->id)->first();
            $val = $key;
            if($history != null){
                $chart = \App\Models\ChartTeamwork::where('id','=',$history->chart_id)->first()->toArray();
                for ($i = 0; $i < 8; $i++) {
                  $data[$i] = $chart[$str[$i]];
                }
                $object = new \stdClass();
                $object->data = $data;
                $object->name = $record->name;;
                $teamwork_result_arr[] = $object;
            }
        }
        $colums = 'BCDEFGHI';
        foreach($teamwork_result_arr as $value){
            $sheet->setCellValue('A' . $rows, $value->name);
            foreach($value->data as $key=>$val){
                $sheet->setCellValue($colums[$key] . $rows, $val);
            }
            $rows++;
        }
        for ($i = 'B'; $i <=  $spreadsheet->getActiveSheet()->getHighestColumn() ; $i++) {
            $spreadsheet->getActiveSheet()->getColumnDimension($i)->setWidth(20);
        }
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $fileName = "Ky_nang_lam_viec_nhom.xlsx";
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save("export/".$fileName);
        header("Content-Type: application/vnd.ms-excel");
        return response()->json(array('link'=>'export/'.$fileName));
    }
    public function exportLeadership(Request $request){
        $company_id = $request->get('company_id');
        $company = $this->companyRepo->find($company_id);
        $members = \App\Models\Member::where('company_id','=',$company->id)->get();
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('B1', 'Phong cách lãnh đạo con người');
        $sheet->setCellValue('C1', 'Phong cách lãnh đạo công việc');
        $rows = 2;
         //Leadership
         $name_arr = [];
         $human_arr = [];
         $work_arr = [];
         foreach($members as $key => $member){
             $chart_leadership = \App\Models\ChartLeadership::where('member_id','=',$member->id)->first();
             if($chart_leadership != null){
                 $name_arr[] = $member->name;
                 $human_arr[] = $chart_leadership->human;
                 $work_arr[] = $chart_leadership->work;
             }
         }
         foreach($name_arr as $key=>$value){
            $sheet->setCellValue('A' . $rows, $value);
            $sheet->setCellValue('B' . $rows, $human_arr[$key]);
            $sheet->setCellValue('C' . $rows, $work_arr[$key]);
            $rows++;
        }
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(40);
        $fileName = "Leadership.xlsx";
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save("export/".$fileName);
        header("Content-Type: application/vnd.ms-excel");
        return response()->json(array('link'=>'export/'.$fileName));
    }
    public function exportPersonalValue(Request $request){
        $company_id = $request->get('company_id');
        $company = $this->companyRepo->find($company_id);
        $members = \App\Models\Member::where('company_id','=',$company->id)->get();
        $personal_value = $this->historyquickRepo->getDataTable($company->id,\App\Models\HistoryTestQuick::TYPE_PERSONAL_VALUE);
        $members_table_personal_value = $personal_value['member_table']->pluck('name')->toArray();
        $questions_personal_value = $personal_value['questions'];
        $answer_avg_personal_value = $personal_value['answer_avg'];
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'STT');
        $sheet->setCellValue('B1', 'Nội dung');
        $sheet->setCellValue('C1', 'AVE');
        $spreadsheet->getActiveSheet()
        ->fromArray(
            $members_table_personal_value,   
            NULL,           
            'D1'                       
        );
        $rows = 2;
         //Leadership
         foreach($questions_personal_value as $key=>$value){
            $sheet->setCellValue('A' . $rows, $key + 1);
            $sheet->setCellValue('B' . $rows, $value->question);
            $sheet->setCellValue('C' . $rows, $answer_avg_personal_value[$key]['avg']);
            $answer = $value->answer;
            $spreadsheet->getActiveSheet()
            ->fromArray(
                $answer,   
                NULL,           
                'D'. $rows                        
            );
            $rows++;
        }
        for ($i = 'D'; $i <=  $spreadsheet->getActiveSheet()->getHighestColumn() ; $i++) {
            $spreadsheet->getActiveSheet()->getColumnDimension($i)->setWidth(20);
        }
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $fileName = "Gia_tri_ca_nhan_cua_nhom.xlsx";
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save("export/".$fileName);
        header("Content-Type: application/vnd.ms-excel");
        return response()->json(array('link'=>'export/'.$fileName));
    }
    public function exportCriticalLevel(Request $request){
        $company_id = $request->get('company_id');
        $company = $this->companyRepo->find($company_id);
        $members = \App\Models\Member::where('company_id','=',$company->id)->get();
        $critical_level = $this->historyquickRepo->getDataTable($company->id,\App\Models\HistoryTestQuick::TYPE_CRITICAL_LEVEL);
        $members_table_critical_level = $critical_level['member_table']->pluck('name')->toArray();
        $questions_critical_level = $critical_level['questions'];
        $answer_avg_critical_level = $critical_level['answer_avg'];
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'STT');
        $sheet->setCellValue('B1', 'Nội dung');
        $sheet->setCellValue('C1', 'AVE');
        $spreadsheet->getActiveSheet()
        ->fromArray(
            $members_table_critical_level,   
            NULL,           
            'D1'                       
        );
        $rows = 2;
         //Leadership
         foreach($questions_critical_level as $key=>$value){
            $sheet->setCellValue('A' . $rows, $key + 1);
            $sheet->setCellValue('B' . $rows, $value->question);
            $sheet->setCellValue('C' . $rows, $answer_avg_critical_level[$key]['avg']);
            $answer = $value->answer;
            $spreadsheet->getActiveSheet()
            ->fromArray(
                $answer,   
                NULL,           
                'D'. $rows                      
            );
            $rows++;
        }
        for ($i = 'D'; $i <=  $spreadsheet->getActiveSheet()->getHighestColumn() ; $i++) {
            $spreadsheet->getActiveSheet()->getColumnDimension($i)->setWidth(20);
        }
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(110);
        $fileName = "Muc_do_quan_trong_cua_nhom.xlsx";
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save("export/".$fileName);
        header("Content-Type: application/vnd.ms-excel");
        return response()->json(array('link'=>'export/'.$fileName));
    }
}

