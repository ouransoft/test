<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class UserController extends Controller
{
    public function __construct(UserRepository $userRepo) {
        $this->userRepo = $userRepo;
        $this->middleware('permission:user-index|user-create|user-edit|user-delete', ['only' => ['index','store']]);
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }
    public function index(){
        $records = $this->userRepo->all();
        return view('backend.user.index',compact('records'));
    }
    public function create(){
        $roles_html = \App\Helpers\StringHelper::getSelectOptions(Role::all());
        return view('backend.user.create',compact('roles_html'));
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required'
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['code'] = Hash::make(time());
        $user = $this->userRepo->create($input);
        $user->assignRole($request->input('roles'));
        return redirect()->route('user.index')
                        ->with('success','Tạo mới tài khoản thành công');
    }
    public function edit($id)
    {
        $record = $this->userRepo->find($id);
        $userRole = $record->roles->pluck('id')->all();
        $roles_html = \App\Helpers\StringHelper::getSelectOptions(Role::all(),$userRole);
        return view('backend.user.edit',compact('record','roles_html'));
    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'roles' => 'required'
        ]);
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }
    
        $user = $this->userRepo->find($id);
        $user->update($input);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));
        return redirect()->route('user.index')
                        ->with('success','User updated successfully');
    }
    public function destroy($id){
        $user = $this->userRepo->find($id);
        $user->delete();
        return redirect()->back()->with('success', 'Xóa thành công');
    }
}
