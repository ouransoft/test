<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\CompanyController;

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/admin', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('admin/user', UserController::class);
    Route::resource('admin/company', CompanyController::class);
    Route::resource('admin/roles', RoleController::class);
    Route::get('/admin/member/index', [MemberController::class, 'index'])->name('member.index');
    Route::delete('/admin/member/delete/{id}', [MemberController::class, 'destroy'])->name('member.destroy');
});
