<?php
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\AuthController;

Route::get('/register', [AuthController::class, 'getRegister'])->name('home.get-register');
Route::post('/register', [AuthController::class, 'postRegister'])->name('home.post-register');
Route::get('/login', [AuthController::class, 'getLogin'])->name('home.get-login');
Route::get('/forgot-password', [AuthController::class, 'forgotPassword']);
Route::post('/login', [AuthController::class, 'postLogin'])->name('home.post-login');
Route::get('/logout', [AuthController::class, 'logout'])->name('home.logout');

Route::group(['middleware' => 'frontend'], function() {
    Route::get('/', [FrontendController::class, 'index'])->name('home.index');
    Route::get('/home/mbti', [FrontendController::class, 'mbti'])->name('home.mbti');
    Route::get('/home/leadership', [FrontendController::class, 'leadership'])->name('home.leadership');
    Route::get('/home/teamwork', [FrontendController::class, 'teamwork'])->name('home.teamwork');
    Route::get('/home/quick', [FrontendController::class, 'quick'])->name('home.quick');
    Route::get('/home/quick2', [FrontendController::class, 'quick2'])->name('home.quick2');
    Route::get('/home/result-quick', [FrontendController::class, 'resultQuick'])->name('home.quick.result');
    Route::get('/home/result', [FrontendController::class, 'resultMbti'])->name('home.mbti.result');
    Route::get('/home/result-leadership', [FrontendController::class, 'resultLeadership'])->name('home.leadership.result');
    Route::get('/home/result-teamwork', [FrontendController::class, 'resultTeamwork'])->name('home.teamwork.result');
    //end messenger
});
