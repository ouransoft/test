<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\MbtiController;
use App\Http\Controllers\Api\LeadershipController;
use App\Http\Controllers\Api\QuickController;
use App\Http\Controllers\Api\TeamworkController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FrontendController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware([])->group(function () {
    Route::post('/save-result', [MbtiController::class, 'saveResult']);
    Route::post('/save-result-teamwork', [TeamworkController::class, 'saveResult']);
    Route::post('/save-result-quick', [QuickController::class, 'saveResultImportant']);
    Route::post('/save-result-quick2', [QuickController::class, 'saveResultImportant']);
    Route::post('/forgot-password', [FrontendController::class, 'forgotPassword']);
    Route::post('/post-forgot-password', [FrontendController::class, 'postForgotPassword'])->name('home.post-forgot-password');;
    Route::post('/save-result-leadership', [LeadershipController::class, 'saveResult']);
    Route::post('/dashboard/company-select', [DashboardController::class, 'companySelectChange']);
    Route::post('/dashboard/search-chart-mbti', [DashboardController::class, 'searchChartMbti']);
    Route::post('/dashboard/search-chart-teamwork', [DashboardController::class, 'searchChartTeamwork']);
    Route::post('/dashboard/export-mbti', [DashboardController::class, 'exportMbti']);
    Route::post('/dashboard/export-teamwork', [DashboardController::class, 'exportTeamwork']);
    Route::post('/dashboard/export-leadership', [DashboardController::class, 'exportLeadership']);
    Route::post('/dashboard/export-personal-value', [DashboardController::class, 'exportPersonalValue']);
    Route::post('/dashboard/export-critical-level', [DashboardController::class, 'exportCriticalLevel']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {

});
